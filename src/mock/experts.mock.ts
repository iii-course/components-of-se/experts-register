import {Expert} from "../app/core/models/expert.model";

export const expertsMock: Expert[] = [
  {
    id: 1,
    firstName:	'Іван',
    lastName:	'Іванов',
    patronymic:	'Іванович',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	true
  },
  {
    id: 2,
    firstName:	'Оксана',
    lastName:	'Іванова',
    patronymic:	'Іванівна',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	false
  },
  {
    id: 1,
    firstName:	'Іван',
    lastName:	'Іванов',
    patronymic:	'Іванович',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	true
  },
  {
    id: 1,
    firstName:	'Іван',
    lastName:	'Іванов',
    patronymic:	'Іванович',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	true
  },
  {
    id: 1,
    firstName:	'Іван',
    lastName:	'Іванов',
    patronymic:	'Іванович',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	true
  },
  {
    id: 1,
    firstName:	'Іван',
    lastName:	'Іванов',
    patronymic:	'Іванович',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	true
  },
  {
    id: 1,
    firstName:	'Іван',
    lastName:	'Іванов',
    patronymic:	'Іванович',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	true
  },
  {
    id: 1,
    firstName:	'Іван',
    lastName:	'Іванов',
    patronymic:	'Іванович',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	true
  },
  {
    id: 1,
    firstName:	'Іван',
    lastName:	'Іванов',
    patronymic:	'Іванович',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	true
  },
  {
    id: 1,
    firstName:	'Іван',
    lastName:	'Іванов',
    patronymic:	'Іванович',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	true
  },
  {
    id: 1,
    firstName:	'Іван',
    lastName:	'Іванов',
    patronymic:	'Іванович',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	true
  },
  {
    id: 2,
    firstName:	'Оксана',
    lastName:	'Іванова',
    patronymic:	'Іванівна',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	false
  },
  {
    id: 2,
    firstName:	'Оксана',
    lastName:	'Іванова',
    patronymic:	'Іванівна',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	false
  },
  {
    id: 2,
    firstName:	'Оксана',
    lastName:	'Іванова',
    patronymic:	'Іванівна',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	false
  },
  {
    id: 2,
    firstName:	'Оксана',
    lastName:	'Іванова',
    patronymic:	'Іванівна',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	false
  },
  {
    id: 2,
    firstName:	'Оксана',
    lastName:	'Іванова',
    patronymic:	'Іванівна',
    contacts: {
      email:	'ivan@example.com',
      phone:	'380981234567',
    },
    organisation: {
      name:	'Одеський науково-дослідний інститут судових експертиз Міністерства юстиції України',
      region:	'Одеська',
      city:	'Одеса',
      street:	'вул. Рішельєвська',
      numberOfBuilding: 8,
    },
    authority: 'Міністерство юстиції України',
    isFromGovernment:	false
  }
];

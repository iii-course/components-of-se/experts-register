import {Request, RequestOperationType, RequestStatus} from "../app/core/models/request.model";
import {usersMock} from "./user.mock";
import {certificatesMock} from "./certificates.mock";
import {expertsMock} from "./experts.mock";
import {commissionsDecisionsMock, commissionsMock} from "./commissions.mock";

export enum RequestsStatuses {
  APPROVED = 'Підтверджено',
  WAITING_FOR_APPROVAL = 'Очікує підтвердження',
  REJECTED = 'Відхилено'
}

export const operationTypesMock: RequestOperationType[] = [
  {
    id: 1,
    operationType: 'Включення даних'
  },
  {
    id: 2,
    operationType: 'Унесення змін'
  }
];

export const requestStatuses: RequestStatus[] = [
  {
    id: 1,
    status: RequestsStatuses.APPROVED
  },
  {
    id: 2,
    status: RequestsStatuses.WAITING_FOR_APPROVAL
  },
  {
    id: 3,
    status: RequestsStatuses.REJECTED
  }
];

export const requestsMock: Request[] = [
  {
    id: 1,
    date: '01-01-2021',
    comment: 'Не правильно заповнено номер свідоцтва',
    operationType: {
      id: 1, operationType: 'Включення даних до Реєстру'
    },
    status: requestStatuses[2],
    certificate: certificatesMock[0],
    expert: expertsMock[0],
    commission: commissionsMock[0],
    commissionDecision: commissionsDecisionsMock[0],
    dismissalOrderDate: '01-02-2021',
    dismissalOrderNumber: 1,
    author: usersMock[0]
  },
  {
    id: 2,
    date: '01-01-2021',
    operationType: {
      id: 1, operationType: 'Включення даних до Реєстру'
    },
    status: requestStatuses[1],
    certificate: certificatesMock[0],
    expert: expertsMock[0],
    commission: commissionsMock[0],
    commissionDecision: commissionsDecisionsMock[0],
    author: usersMock[0]
  },
  {
    id: 2,
    date: '01-01-2021',
    operationType: {
      id: 1, operationType: 'Включення даних до Реєстру'
    },
    status: requestStatuses[0],
    certificate: certificatesMock[0],
    expert: expertsMock[0],
    commission: commissionsMock[0],
    commissionDecision: commissionsDecisionsMock[0],
    author: usersMock[0]
  },
  {
    id: 2,
    date: '01-01-2021',
    operationType: {
      id: 1, operationType: 'Включення даних до Реєстру'
    },
    status: requestStatuses[0],
    certificate: certificatesMock[0],
    expert: expertsMock[0],
    commission: commissionsMock[0],
    commissionDecision: commissionsDecisionsMock[0],
    author: usersMock[0]
  },
];

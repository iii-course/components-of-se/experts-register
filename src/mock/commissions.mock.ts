import {Commission, CommissionDecision, CommissionDecisionType} from "../app/core/models/commission.model";
import {authoritiesMock} from "./authorities.mock";

export const commissionDecisionTypesMock: CommissionDecisionType[] = [
  {
    id: 1,
    type: "Присвоєння кваліфікації судового експерта"
  },
  {
    id: 2,
    type: "Підтвердження кваліфікації судового експерта"
  },
  {
    id: 3,
    type: "Підвищення кваліфікації судового експерта"
  }
];

export const commissionsDecisionsMock: CommissionDecision[] = [
  {
    number: 1,
    date: "10-10-2021",
    type: {
      id: 2,
      type: "Присвоєння кваліфікації судового експерта"
    }
  },
  {
    number: 1,
    date: "10-10-2021",
    type: {
      id: 2,
      type: "Присвоєння кваліфікації судового експерта"
    }
  },
  {
    number: 1,
    date: "10-10-2021",
    type: {
      id: 2,
      type: "Присвоєння кваліфікації судового експерта"
    }
  },
  {
    number: 1,
    date: "10-10-2021",
    type: {
      id: 2,
      type: "Присвоєння кваліфікації судового експерта"
    }
  },
  {
    number: 1,
    date: "10-10-2021",
    type: {
      id: 2,
      type: "Присвоєння кваліфікації судового експерта"
    }
  }
];

export const commissionsMock: Commission[] = [
  {
    name:	"Експертно-кваліфікаційна комісія",
    authority: authoritiesMock[0]
  },
  {
    name:	"Експертно-кваліфікаційна комісія",
    authority: authoritiesMock[0]
  },
  {
    name:	"Експертно-кваліфікаційна комісія",
    authority: authoritiesMock[0]
  },
  {
    name:	"Експертно-кваліфікаційна комісія",
    authority: authoritiesMock[0]
  }
];

import {ExpertiseClass, ExpertiseType} from "../app/core/models/expertise.model";

export const expertiseTypesMock: ExpertiseType[] = [
  {
    id:	1,
    number: 4,
    name: "Трасологічна",
    expertiseSpecialities: [
      {
        id:	1,
        number: 1,
        name: "Дослідження слідів людини та слідів тварини",
      },
      {
        id:	2,
        number: 2,
        name: "Дослідження знарядь, агрегатів, інструментів і залишених ними слідів, ідентифікація цілого за частинами",
      },
      {
        id:	3,
        number: 3,
        name: "Криміналістичне дослідження транспортних засобів",
      },
      {
        id:	4,
        number: 4,
        name: "Дослідження ідентифікаційних номерів та рельєфних знаків",
      }
    ]
  },
  {
    id:	2,
    number: 4,
    name: "Інженерно-транспортна",
    expertiseSpecialities: [
      {
        id:	5,
        number: 1,
        name: "Дослідження обставин і механізму дорожньо-транспортних пригод",
      },
      {
        id:	6,
        number: 2,
        name: "Дослідження технічного стану транспортних засобів",
      },
      {
        id:	7,
        number: 3,
        name: "Дослідження деталей транспортних засобів",
      },
      {
        id:	8,
        number: 4,
        name: "Транспортно-трасологічні дослідження",
      }
    ]
  },
  {
    id:	3,
    number: 4,
    name: "Безпеки життєдіяльності",
    expertiseSpecialities: [
      {
        id:	9,
        number: 1,
        name: "Дослідження причин та наслідків порушення вимог безпеки життєдіяльності та охорони праці",
      },
    ]
  }
];

export const expertiseClassesMock: ExpertiseClass[] = [
  {
    id:	1,
    classNumber: 1,
    name: 'Криміналістична експертиза',
    expertiseTypes: [expertiseTypesMock[0]],
  },
  {
    id:	2,
    classNumber: 2,
    name: 'Інженерно-технічна експертиза',
    expertiseTypes: [expertiseTypesMock[1]],
  },
  {
    id:	3,
    classNumber: 3,
    name: 'Економічна експертиза',
    expertiseTypes: [expertiseTypesMock[2]],
  },
];

import { User } from 'src/app/core/models/user.model';
import { passportDataMock } from './passport-data.mock';
import {userRolesMock} from "./user-role.mock";

export const userMock: User = {
  id: 1,
  role: userRolesMock[0],
  firstName: 'Іван',
  lastName: 'Іванов',
  patronymic: 'Іванович',
  birthdate: '01-01-1970',
  passport: passportDataMock,
  taxpayerRegistrationNumber: '1234567890',
  email: 'example@email.com',
  password: 'string',
  isActive: true
};

export const usersMock: User[] = [
  {...userMock, role: userRolesMock[1]}, {...userMock, isActive: false}, userMock, userMock, userMock,
  userMock, userMock, userMock, userMock, userMock,
  userMock, userMock, userMock, userMock, userMock
];

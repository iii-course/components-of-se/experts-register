import {Log, TypeOfChange} from "../app/core/models/log.model";
import {userRolesMock} from "./user-role.mock";

export const typesOfChangesMock: TypeOfChange[] = [
  {
    id: 1,
    name: "Прийняття запиту"
  },
  {
    id: 2,
    name: "Відхилення запиту"
  },
  {
    id: 3,
    name: "Подача запиту"
  },
  {
    id: 4,
    name: "Реєстрація"
  },
  {
    id: 5,
    name: "Редагування акаунту"
  }
];

export const logsMock: Log[] = [
  {
    id: 1,
    author: {
      id: 1,
      role: userRolesMock[0],
      firstName: "Іван",
      lastName: "Іванов",
      patronymic: "Іванович",
      email: "example@email.com",
    },
    affectedUser: {
      id: 2,
      role: userRolesMock[1],
      firstName: "Оксана",
      lastName: "Іванова",
      patronymic: "Іванівна",
      email: "example@email.com",
    },
    date: "01-02-2021",
    typeOfChange: {
      id: 1,
      name: "Прийняття запиту"
    },
    request: {
      id: 1,
      date: "01-01-2021",
      operationType: "Включення даних до Реєстру"
    }
  },
  {
    id: 1,
    author: {
      id: 1,
      role: userRolesMock[0],
      firstName: "Іван",
      lastName: "Іванов",
      patronymic: "Іванович",
      email: "example@email.com",
    },
    affectedUser: {
      id: 2,
      role: userRolesMock[1],
      firstName: "Оксана",
      lastName: "Іванова",
      patronymic: "Іванівна",
      email: "example@email.com",
    },
    date: "01-02-2021",
    typeOfChange: {
      id: 1,
      name: "Прийняття запиту"
    },
    request: {
      id: 1,
      date: "01-01-2021",
      operationType: "Включення даних до Реєстру"
    }
  },
  {
    id: 1,
    author: {
      id: 1,
      role: userRolesMock[0],
      firstName: "Іван",
      lastName: "Іванов",
      patronymic: "Іванович",
      email: "example@email.com",
    },
    affectedUser: {
      id: 2,
      role: userRolesMock[1],
      firstName: "Оксана",
      lastName: "Іванова",
      patronymic: "Іванівна",
      email: "example@email.com",
    },
    date: "01-02-2021",
    typeOfChange: {
      id: 1,
      name: "Прийняття запиту"
    },
    request: {
      id: 1,
      date: "01-01-2021",
      operationType: "Включення даних до Реєстру"
    }
  },
  {
    id: 1,
    author: {
      id: 1,
      role: userRolesMock[0],
      firstName: "Іван",
      lastName: "Іванов",
      patronymic: "Іванович",
      email: "example@email.com",
    },
    affectedUser: {
      id: 2,
      role: userRolesMock[1],
      firstName: "Оксана",
      lastName: "Іванова",
      patronymic: "Іванівна",
      email: "example@email.com",
    },
    date: "01-02-2021",
    typeOfChange: {
      id: 1,
      name: "Прийняття запиту"
    },
    request: {
      id: 1,
      date: "01-01-2021",
      operationType: "Включення даних до Реєстру"
    }
  },
  {
    id: 1,
    author: {
      id: 1,
      role: userRolesMock[0],
      firstName: "Іван",
      lastName: "Іванов",
      patronymic: "Іванович",
      email: "example@email.com",
    },
    affectedUser: {
      id: 2,
      role: userRolesMock[1],
      firstName: "Оксана",
      lastName: "Іванова",
      patronymic: "Іванівна",
      email: "example@email.com",
    },
    date: "01-02-2021",
    typeOfChange: {
      id: 1,
      name: "Прийняття запиту"
    },
    request: {
      id: 1,
      date: "01-01-2021",
      operationType: "Включення даних до Реєстру"
    }
  },
  {
    id: 1,
    author: {
      id: 1,
      role: userRolesMock[0],
      firstName: "Іван",
      lastName: "Іванов",
      patronymic: "Іванович",
      email: "example@email.com",
    },
    affectedUser: {
      id: 2,
      role: userRolesMock[1],
      firstName: "Оксана",
      lastName: "Іванова",
      patronymic: "Іванівна",
      email: "example@email.com",
    },
    date: "01-02-2021",
    typeOfChange: {
      id: 1,
      name: "Прийняття запиту"
    },
    request: {
      id: 1,
      date: "01-01-2021",
      operationType: "Включення даних до Реєстру"
    }
  },
  {
    id: 1,
    author: {
      id: 1,
      role: userRolesMock[0],
      firstName: "Іван",
      lastName: "Іванов",
      patronymic: "Іванович",
      email: "example@email.com",
    },
    affectedUser: {
      id: 2,
      role: userRolesMock[1],
      firstName: "Оксана",
      lastName: "Іванова",
      patronymic: "Іванівна",
      email: "example@email.com",
    },
    date: "01-02-2021",
    typeOfChange: {
      id: 1,
      name: "Прийняття запиту"
    },
    request: {
      id: 1,
      date: "01-01-2021",
      operationType: "Включення даних до Реєстру"
    }
  },
  {
    id: 1,
    author: {
      id: 1,
      role: userRolesMock[0],
      firstName: "Іван",
      lastName: "Іванов",
      patronymic: "Іванович",
      email: "example@email.com",
    },
    affectedUser: {
      id: 2,
      role: userRolesMock[1],
      firstName: "Оксана",
      lastName: "Іванова",
      patronymic: "Іванівна",
      email: "example@email.com",
    },
    date: "01-02-2021",
    typeOfChange: {
      id: 1,
      name: "Прийняття запиту"
    },
    request: {
      id: 1,
      date: "01-01-2021",
      operationType: "Включення даних до Реєстру"
    }
  },
  {
    id: 1,
    author: {
      id: 1,
      role: userRolesMock[0],
      firstName: "Іван",
      lastName: "Іванов",
      patronymic: "Іванович",
      email: "example@email.com",
    },
    affectedUser: {
      id: 2,
      role: userRolesMock[1],
      firstName: "Оксана",
      lastName: "Іванова",
      patronymic: "Іванівна",
      email: "example@email.com",
    },
    date: "01-02-2021",
    typeOfChange: {
      id: 1,
      name: "Прийняття запиту"
    },
    request: {
      id: 1,
      date: "01-01-2021",
      operationType: "Включення даних до Реєстру"
    }
  }
];

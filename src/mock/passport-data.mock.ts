import { PassportData } from 'src/app/core/models/passport-data.model';

export const passportDataMock: PassportData = {
    number:	'123456789',
    series: '11',
    dateOfIssue: '01-01-1986',
    authorityCode: '1011',
};

import {Certificate} from "../app/core/models/certificate.model";
import {authoritiesMock} from "./authorities.mock";
import {expertiseTypesMock} from "./expertise.mock";
import {commissionsMock} from "./commissions.mock";

export const certificatesMock: Certificate[] = [
  {
    id: 1,
    documentNumber: 123,
    validityEndDate: "10-10-2022",
    expertiseSpecialities: [
      {
        id:	1,
        number: 1,
        name: "Дослідження слідів людини та слідів тварини",
      },
      {
        id:	1,
        number: 2,
        name: "Дослідження знарядь, агрегатів, інструментів і залишених ними слідів, ідентифікація цілого за частинами",
      }
    ],
    expertiseType: expertiseTypesMock[0],
    commissionDecision: {
      number: 1,
      date: "10-10-2021",
      type: {
        id: 2,
        type: "Присвоєння кваліфікації судового експерта"
      }
    },
    commission: commissionsMock[0]
  },
  {
    id: 1,
    documentNumber: 123,
    validityEndDate: "10-10-2022",
    expertiseSpecialities: [
      {
        id:	1,
        number: 1,
        name: "Дослідження слідів людини та слідів тварини",
      },
      {
        id:	1,
        number: 2,
        name: "Дослідження знарядь, агрегатів, інструментів і залишених ними слідів, ідентифікація цілого за частинами",
      }
    ],
    expertiseType: expertiseTypesMock[0],
    commissionDecision: {
      number: 1,
      date: "10-10-2021",
      type: {
        id: 2,
        type: "Присвоєння кваліфікації судового експерта"
      }
    },
    commission: {
      name:	"Експертно-кваліфікаційна комісія",
      authority: authoritiesMock[0]
    }
  },
  {
    id: 1,
    documentNumber: 123,
    validityEndDate: "10-10-2022",
    expertiseSpecialities: [
      {
        id:	1,
        number: 1,
        name: "Дослідження слідів людини та слідів тварини",
      },
      {
        id:	1,
        number: 2,
        name: "Дослідження знарядь, агрегатів, інструментів і залишених ними слідів, ідентифікація цілого за частинами",
      }
    ],
    expertiseType: expertiseTypesMock[0],
    commissionDecision: {
      number: 1,
      date: "10-10-2021",
      type: {
        id: 2,
        type: "Присвоєння кваліфікації судового експерта"
      }
    },
    commission: {
      name:	"Експертно-кваліфікаційна комісія",
      authority: authoritiesMock[0]
    }
  },
  {
    id: 1,
    documentNumber: 123,
    validityEndDate: "10-10-2022",
    expertiseSpecialities: [
      {
        id:	1,
        number: 1,
        name: "Дослідження слідів людини та слідів тварини",
      },
      {
        id:	1,
        number: 2,
        name: "Дослідження знарядь, агрегатів, інструментів і залишених ними слідів, ідентифікація цілого за частинами",
      }
    ],
    expertiseType: expertiseTypesMock[0],
    commissionDecision: {
      number: 1,
      date: "10-10-2021",
      type: {
        id: 2,
        type: "Присвоєння кваліфікації судового експерта"
      }
    },
    commission: {
      name:	"Експертно-кваліфікаційна комісія",
      authority: authoritiesMock[0]
    }
  }
];

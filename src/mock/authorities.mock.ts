import {Authority} from "../app/core/models/authority.model";

export const authoritiesMock: Authority[] = [
  {
    id: 1,
    name: 'Міністерство юстиції України'
  },
  {
    id: 2,
    name: 'Міністерство охорони здоров’я України'
  },
  {
    id: 3,
    name: 'Міністерство внутрішніх справ України'
  },
  {
    id: 4,
    name: 'Міністерство оборони України'
  }
];

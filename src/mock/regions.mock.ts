import {Region} from "../app/core/models/region.model";

export const regionsMock: Region[] = [
  {
    id: 1,
    name: 'Київська'
  },
  {
    id: 2,
    name: 'Житомирська'
  }
];

import { UserRole, UserRolesNames } from 'src/app/core/models/user-role.model';

export const userRoleAdministratorMock: UserRole = {
    id: 1,
    name: UserRolesNames.ADMIN
};

export const userRoleApplicantExpertMock: UserRole = {
    id: 2,
    name: UserRolesNames.APPLICANT_EXPERT
};

export const userRoleApplicantHeadMock: UserRole = {
  id: 2,
  name: UserRolesNames.APPLICANT_HEAD
};

export const userRoleApplicantCentralCommissionHeadMock: UserRole = {
  id: 2,
  name: UserRolesNames.APPLICANT_CENTRAL_COMMISSION_HEAD
};


export const userRoleRegistarMock: UserRole = {
    id: 3,
    name: UserRolesNames.REGISTRAR
};

export const userRolesMock: UserRole[] = [
  userRoleAdministratorMock,
  userRoleApplicantExpertMock,
  userRoleApplicantHeadMock,
  userRoleApplicantCentralCommissionHeadMock,
  userRoleRegistarMock
];

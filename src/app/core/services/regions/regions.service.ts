import {Injectable} from "@angular/core";
import {ApiService} from "../api/api.service";
import {Observable, of} from "rxjs";
import {Region} from "../../models/region.model";
import {regionsMock} from "../../../../mock/regions.mock";
import {regionsUrl} from "../api/paths";

@Injectable({
  providedIn: 'root'
})
export class RegionsService {

  constructor(private apiService: ApiService) { }

  public getAllRegions(): Observable<Region[]> {
    return this.apiService.get<Region[]>(regionsUrl);
  }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserRole } from '../../models/user-role.model';
import {ApiService} from "../api/api.service";
import {User, UsersSearchTerm} from "../../models/user.model";
import {userRolesUrl, usersTotalCountUrl, usersUrl} from "../api/paths";
import {AuthService} from "../auth/auth.service";
import {map, switchMap} from "rxjs/operators";
import {getDateStringFormatted} from "../../../helpers/dates";

function userMapper(user: User): User {
  return {
    ...user,
    birthdate: getDateStringFormatted(user.birthdate),
    passport: {...user.passport, dateOfIssue:getDateStringFormatted(user.passport.dateOfIssue) }
  };
}

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private apiService: ApiService, private authService: AuthService) { }

  public getAllUsers(params?: UsersSearchTerm): Observable<User[]> {
    return this.apiService.get<User[]>(usersUrl, {params});
  }

  public getUsersTotalCount(params?: UsersSearchTerm): Observable<number> {
    return this.apiService.get<number>(usersTotalCountUrl, {params});
  }

  public getUsersRoles(): Observable<UserRole[]> {
    return this.apiService.get<UserRole[]>(userRolesUrl);
  }

  public getUserById(id: number): Observable<User> {
    return this.apiService.get<User>(`${usersUrl}/${id}`).pipe(map(userMapper));
  }

  public updateUserById(id: number, data: Partial<User>): Observable<User> {
    return this.apiService.patch<User>(`${usersUrl}/${id}`, data).pipe(map(userMapper));
  }

  public addNewUser(data: Partial<User>): Observable<void> {
    return this.authService.register(data);
  }

  public getCurrentUser(): Observable<User> {
    return this.authService.userId$.pipe(
      switchMap(id => this.getUserById(id)),
      map(userMapper)
    );
  }
}

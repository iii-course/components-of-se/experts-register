import { UsersService } from './users.service';
import {MockService} from "ng-mocks";
import {ApiService} from "../api/api.service";
import {AuthService} from "../auth/auth.service";

describe('UsersService', () => {
  let service: UsersService;

  beforeEach(() => {
    service = new UsersService(MockService(ApiService), MockService(AuthService));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

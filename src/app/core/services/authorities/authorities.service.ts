import { Injectable } from '@angular/core';
import {ApiService} from "../api/api.service";
import {Observable} from "rxjs";
import {Authority} from "../../models/authority.model";
import {authoritiesUrl} from "../api/paths";

@Injectable({
  providedIn: 'root'
})
export class AuthoritiesService {

  constructor(private apiService: ApiService) { }

  public getAllAuthorities(): Observable<Authority[]> {
    return this.apiService.get<Authority[]>(authoritiesUrl);
  }
}

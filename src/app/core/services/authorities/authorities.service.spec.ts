import { AuthoritiesService } from './authorities.service';
import {MockService} from "ng-mocks";
import {ApiService} from "../api/api.service";

describe('AuthoritiesService', () => {
  let service: AuthoritiesService;

  beforeEach(() => {
    service = new AuthoritiesService(MockService(ApiService));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

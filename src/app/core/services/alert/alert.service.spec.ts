import { AlertService } from './alert.service';
import {MockService} from "ng-mocks";
import {TranslocoService} from "@ngneat/transloco";

describe('AlertService', () => {
  let service: AlertService;

  beforeEach(() => {
    service = new AlertService(MockService(TranslocoService));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

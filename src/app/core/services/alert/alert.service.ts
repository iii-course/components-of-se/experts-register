import { Injectable } from '@angular/core';
import {TranslocoService} from "@ngneat/transloco";
import {HttpErrorResponse} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  private defaultErrorMessage = 'error_has_happened';

  constructor(private translocoService: TranslocoService) {}

  public alertSuccessfulAccountUpdate(): void {
    this.alertWithTranslation('account_updated_successfully');
  }

  public alertSuccessfulRegistration(): void {
    this.alertWithTranslation('account_created_successfully');
  }

  public alertSuccessfulRequestCreation(): void {
    this.alertWithTranslation('request_created_successfully');
  }

  public alertSuccessfulRequestApproval(): void {
    this.alertWithTranslation('request_approved_successfully');
  }

  public alertSuccessfulRequestRejection(): void {
    this.alertWithTranslation('request_rejected_successfully');
  }

  public alertIncorrectComment(): void {
    this.alertWithTranslation('incorrect_request_rejection_comment');
  }

  public alertInvalidCredentialsError(): void {
    this.alertWithTranslation('invalid_credentials_error');
  }

  public alertError(err: Error): void {
    const defaultMessage = this.translocoService.translate(this.defaultErrorMessage);
    alert((err as HttpErrorResponse)?.error?.message || err?.message || defaultMessage);
  }

  private alertWithTranslation(key: string) {
    alert(this.translocoService.translate(key));
  }
}

export const expertsUrl = '/api/experts';
export const expertsTotalCountUrl = '/api/experts/totalCount';

export const authoritiesUrl = '/api/expertise/authorities';
export const expertiseClassesUrl = '/api/expertise/classes';

export const usersUrl = '/api/users';
export const usersTotalCountUrl = '/api/users/totalCount';
export const userRolesUrl = '/api/users/roles';

export const loginUrl = '/api/auth/login';
export const registerUrl = '/api/auth/register';

export const logsTypesOfChangesUrl = '/api/logs/typesOfChanges';
export const logsUrl = '/api/logs';
export const logsTotalCountUrl = '/api/logs/totalCount';

export const requestsUrl = '/api/requests';
export const requestsTotalCountUrl = '/api/requests/totalCount';
export const requestStatusesUrl = '/api/requests/statuses';
export const requestsOperationTypesUrl = '/api/requests/operationTypes';

export const commissionDecisionTypesUrl = '/api/expertise/commissions/decisions/types';

export const regionsUrl = '/api/regions';

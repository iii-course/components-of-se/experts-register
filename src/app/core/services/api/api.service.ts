import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {shareReplay} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient) { }

  public get<T>(url: string, options?: object): Observable<T> {
    return this.http.get<T>(formWholeUrl(url), options).pipe(
      shareReplay({bufferSize: 1, refCount: true})
    );
  }

  public post<T>(url: string, data: object, options?: object): Observable<T> {
    return this.http.post<T>(formWholeUrl(url), data, options).pipe(
      shareReplay({bufferSize: 1, refCount: true})
    );
  }

  public patch<T>(url: string, data: object, options?: object): Observable<T> {
    return this.http.patch<T>(formWholeUrl(url), data, options).pipe(
      shareReplay({bufferSize: 1, refCount: true})
    );
  }

  public delete<T>(url: string, options?: object): Observable<T> {
    return this.http.delete<T>(formWholeUrl(url), options).pipe(
      shareReplay({bufferSize: 1, refCount: true})
    );
  }
}

function formWholeUrl(urlPart: string): string {
  if (urlPart[0] === '/') {
    return `${environment.apiUrl}${urlPart}`;
  }
  return `${environment.apiUrl}/${urlPart}`;
}

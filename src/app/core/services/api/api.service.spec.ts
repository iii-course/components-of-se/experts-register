import { ApiService } from './api.service';
import {MockService} from "ng-mocks";
import {HttpClient} from "@angular/common/http";

describe('ApiService', () => {
  let service: ApiService;

  beforeEach(() => {
    service = new ApiService(MockService(HttpClient));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

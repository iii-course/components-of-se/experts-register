import { Injectable } from '@angular/core';
import {ApiService} from "../api/api.service";
import {Observable, of} from "rxjs";
import {Expert, ExpertsSearchTerm} from "../../models/expert.model";
import {expertsTotalCountUrl, expertsUrl} from "../api/paths";
import {Certificate} from "../../models/certificate.model";

@Injectable({
  providedIn: 'root'
})
export class ExpertsService {

  constructor(private apiService: ApiService) { }

  public getAllExperts(params?: ExpertsSearchTerm): Observable<Expert[]> {
    return this.apiService.get<Expert[]>(expertsUrl, {params});
  }

  public getExpertsTotalCount(params?: ExpertsSearchTerm): Observable<number> {
    return this.apiService.get<number>(expertsTotalCountUrl, {params});
  }

  public getExpertCertificates(expertId: number): Observable<Certificate[]> {
    return this.apiService.get<Certificate[]>(`${expertsUrl}/${expertId}/certificates`);
  }
}

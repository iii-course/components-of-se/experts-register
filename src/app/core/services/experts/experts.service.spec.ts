import { ExpertsService } from './experts.service';
import {MockService} from "ng-mocks";
import {ApiService} from "../api/api.service";

describe('ExpertsService', () => {
  let service: ExpertsService;

  beforeEach(() => {
    service = new ExpertsService(MockService(ApiService));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

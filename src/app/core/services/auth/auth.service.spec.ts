import { AuthService } from './auth.service';
import {MockService} from "ng-mocks";
import {ApiService} from "../api/api.service";
import {Router} from "@angular/router";
import {LocalStorageService} from "../local-storage/local-storage.service";

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(() => {
    service = new AuthService(
      MockService(ApiService),
      MockService(Router),
      MockService(LocalStorageService)
    );
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

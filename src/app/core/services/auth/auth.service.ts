import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { Credentials } from '../../models/credentials.model';
import { UserRole } from '../../models/user-role.model';
import { User, UserAuthData } from '../../models/user.model';
import { ApiService } from '../api/api.service';
import { LocalStorageService } from '../local-storage/local-storage.service';
import {loginUrl, registerUrl} from "../api/paths";
import {isDefined} from "../../../helpers/rxjs";


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private storageKey = 'user';
  private userAuthDataSubject$ = new BehaviorSubject<UserAuthData | undefined>(undefined);

  constructor(
    private apiService: ApiService,
    private router: Router,
    private localStorageService: LocalStorageService
  ) {
    const userData = this.getUserFromLocalStorage();
    this.userAuthDataSubject$.next(userData);
  }

  public login(credentials: Credentials): Observable<UserAuthData> {
    return this.apiService.post<UserAuthData>(loginUrl, credentials).pipe(
      tap((userData: UserAuthData) => {
        console.log(userData);
        this.setUserData(userData);
        void this.router.navigateByUrl('/');
      })
    );
  }

  public register(userData: Partial<User>): Observable<void> {
    return this.apiService.post(registerUrl, userData);
  }

  public logout(): void {
    this.removeUserFromLocalStorage();
    this.userAuthDataSubject$.next(undefined);
    void this.router.navigateByUrl('/login');
  }

  public get user(): UserAuthData | undefined {
    return this.userAuthDataSubject$.value;
  }

  public get isUserLoggedIn$(): Observable<boolean> {
    return this.userAuthData$.pipe(
      map(user => !!user && !!user.jwt_token && !!user.role)
    );
  }

  public get userAuthData$(): Observable<UserAuthData | undefined> {
    return this.userAuthDataSubject$.asObservable();
  }

  public get userId$(): Observable<number> {
    return this.userAuthData$.pipe(
      map(user => user && user.id),
      filter(isDefined)
    );
  }

  public get userRole$(): Observable<UserRole> {
    return this.userAuthData$.pipe(
      map(user => user && user.role),
      filter(isDefined)
    );
  }

  private setUserData(data: UserAuthData) {
    this.userAuthDataSubject$.next(data);
    this.setUserToLocalStorage(data);
  }

  private setUserToLocalStorage(data: UserAuthData): void {
    this.localStorageService.setItem(this.storageKey, data);
  }

  private getUserFromLocalStorage(): UserAuthData | undefined {
    return this.localStorageService.getItem<UserAuthData>(this.storageKey);
  }

  private removeUserFromLocalStorage(): void {
    this.localStorageService.removeItem(this.storageKey);
  }
}

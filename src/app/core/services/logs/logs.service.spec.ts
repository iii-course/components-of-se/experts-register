import { LogsService } from './logs.service';
import {MockService} from "ng-mocks";
import {ApiService} from "../api/api.service";

describe('LogsService', () => {
  let service: LogsService;

  beforeEach(() => {
    service = new LogsService(MockService(ApiService));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Log, LogsSearchTerm, TypeOfChange} from "../../models/log.model";
import {ApiService} from "../api/api.service";
import {logsTotalCountUrl, logsTypesOfChangesUrl, logsUrl} from "../api/paths";
import {getDateStringFormatted} from "../../../helpers/dates";
import {map} from "rxjs/operators";

function logMapper(log: Log): Log {
  return {...log, date: getDateStringFormatted(log.date)};
}

@Injectable({
  providedIn: 'root'
})
export class LogsService {

  constructor(private apiService: ApiService) { }

  public getTypesOfChanges(): Observable<TypeOfChange[]> {
    return this.apiService.get<TypeOfChange[]>(logsTypesOfChangesUrl);
  }

  public getAllLogs(params?: LogsSearchTerm): Observable<Log[]> {
    return this.apiService.get<Log[]>(logsUrl, {params}).pipe(
      map(logs => logs.map(logMapper))
    );
  }

  public getLogsTotalCount(params?: LogsSearchTerm): Observable<number> {
    return this.apiService.get<number>(logsTotalCountUrl, {params});
  }
}


import { ExpertiseService } from './expertise.service';
import {MockService} from "ng-mocks";
import {ApiService} from "../api/api.service";

describe('ExpertiseService', () => {
  let service: ExpertiseService;

  beforeEach(() => {
    service = new ExpertiseService(MockService(ApiService));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

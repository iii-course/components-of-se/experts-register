import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {ExpertiseClass, ExpertiseSpeciality, ExpertiseType} from "../../models/expertise.model";
import {ApiService} from "../api/api.service";
import {expertiseClassesMock} from "../../../../mock/expertise.mock";
import {map, tap} from "rxjs/operators";
import {expertiseClassesUrl} from "../api/paths";

@Injectable({
  providedIn: 'root'
})
export class ExpertiseService {
  private expertiseClasses$!: Observable<ExpertiseClass[]>;

  constructor(private apiService: ApiService) { }

  public getAllExpertiseClasses(): Observable<ExpertiseClass[]> {
    if (this.expertiseClasses$) {
      return this.expertiseClasses$;
    }
    return this.apiService.get<ExpertiseClass[]>(expertiseClassesUrl).pipe(
      tap(classes => this.expertiseClasses$ = of(classes))
    );
  }

  public getAllExpertiseTypes(classId?: number): Observable<ExpertiseType[]> {
    return this.getAllExpertiseClasses().pipe(
      map(classes => {
        if (classId) {
          return classes.filter(c => c.id === classId);
        }
        return classes;
      }),
      map(classes => classes.map(c => c.expertiseTypes)),
      map(types => types.flat())
    );
  }

  public getAllExpertiseSpecialities(typeId?: number): Observable<ExpertiseSpeciality[]> {
    return this.getAllExpertiseTypes().pipe(
      map(types => {
        if (typeId) {
          return types.filter(t => t.id === typeId);
        }
        return types;
      }),
      map(types => types.map(t => t.expertiseSpecialities)),
      map(specialities => specialities.flat())
    );
  }
}

import { RequestsService } from './requests.service';
import {MockService} from "ng-mocks";
import {ApiService} from "../api/api.service";
import {AuthService} from "../auth/auth.service";

describe('RequestsService', () => {
  let service: RequestsService;

  beforeEach(() => {
    service = new RequestsService(MockService(ApiService), MockService(AuthService));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import {ApiService} from "../api/api.service";
import {Observable, of} from "rxjs";
import {RequestOperationType, RequestPostData, RequestsSearchTerm, RequestStatus} from "../../models/request.model";
import {Request} from "../../models/request.model";
import {switchMap, take, tap, map} from "rxjs/operators";
import {AuthService} from "../auth/auth.service";
import {requestsOperationTypesUrl, requestStatusesUrl, requestsTotalCountUrl, requestsUrl} from "../api/paths";
import {getDateStringFormatted} from "../../../helpers/dates";

function requestMapper(request: Request): Request {
  return {...request,
    date: getDateStringFormatted(request.date),
    commissionDecision: {
      ...request.commissionDecision,
      date: getDateStringFormatted(request.commissionDecision.date)
    },
    certificate: {
      ...request.certificate,
      validityEndDate: getDateStringFormatted(request.certificate.validityEndDate)
    },
    dismissalOrderDate: getDateStringFormatted(request.dismissalOrderDate)
  };
}

@Injectable({
  providedIn: 'root'
})
export class RequestsService {
  private requestStatuses!: RequestStatus[];
  private requestOperationTypes!: RequestOperationType[];

  constructor(
    private apiService: ApiService,
    private authService: AuthService
  ) {
    this.prefetchData();
  }

  public getRequestById(id: number): Observable<Request> {
    return this.apiService.get<Request>(`${requestsUrl}/${id}`).pipe(
      map(requestMapper)
    );
  }

  public getAllRequests(params?: RequestsSearchTerm): Observable<Request[]> {
    return this.apiService.get<Request[]>(requestsUrl, {params}).pipe(
      map(requests => requests.map(requestMapper))
    );
  }

  public getRequestsCount(params?: RequestsSearchTerm): Observable<number> {
    return this.apiService.get<number>(requestsTotalCountUrl, {params});
  }

  public getAllRequestsStatuses(): Observable<RequestStatus[]> {
    if (this.requestStatuses && this.requestStatuses.length) {
      return of(this.requestStatuses);
    }
    return this.apiService.get<RequestStatus[]>(requestStatusesUrl).pipe(
      tap(statuses => this.requestStatuses = statuses)
    );
  }

  public getAllRequestOperationTypes(): Observable<RequestOperationType[]> {
    if (this.requestOperationTypes) {
      return of(this.requestOperationTypes);
    }
    return this.apiService.get<RequestOperationType[]>(requestsOperationTypesUrl).pipe(
      tap(types => this.requestOperationTypes = types)
    );
  }

  public getOperationTypeId(operationTypeName: string): number | undefined {
    return this.requestOperationTypes.find(type => type.operationType === operationTypeName)?.id;
  }

  public getOperationTypeName(operationTypeId: number): string | undefined {
    return this.requestOperationTypes.find(type => type.id === operationTypeId)?.operationType;
  }

  public getStatusId(statusName: string): Observable<number | undefined> {
    return this.getAllRequestsStatuses().pipe(
      map(statuses => statuses.find(status => status.status === statusName)?.id)
    );
  }

  public addNewRequest(data: RequestPostData): Observable<any> {
    return this.authService.userId$.pipe(
      map(id => ({...data, authorId: id})),
       switchMap(params => this.apiService.post(requestsUrl, params))
    );
  }

  public updateRequestStatusById(requestId: number, data: {statusId?: number, comment?: string}): Observable<any> {
    return this.apiService.patch(`${requestsUrl}/${requestId}`, data);
  }

  private prefetchData(): void {
    this.getAllRequestsStatuses().pipe(take(1)).subscribe();
    this.getAllRequestOperationTypes().pipe(take(1)).subscribe();
  }
}

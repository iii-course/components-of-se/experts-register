import { Injectable } from '@angular/core';
import {ApiService} from "../api/api.service";
import {Observable, of} from "rxjs";
import {CommissionDecisionType} from "../../models/commission.model";
import {tap} from "rxjs/operators";
import {commissionDecisionTypesUrl} from "../api/paths";

@Injectable({
  providedIn: 'root'
})
export class CommissionsService {
  private commissionDecisionTypes!: CommissionDecisionType[];

  constructor(private apiService: ApiService) { }

  public getAllCommissionDecisionTypes(): Observable<CommissionDecisionType[]> {
    if (this.commissionDecisionTypes) {
      return of(this.commissionDecisionTypes);
    }
    return this.apiService.get<CommissionDecisionType[]>(commissionDecisionTypesUrl).pipe(
      tap(types => this.commissionDecisionTypes = types)
    );
  }
}

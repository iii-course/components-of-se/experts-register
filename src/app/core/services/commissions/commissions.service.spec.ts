import { CommissionsService } from './commissions.service';
import {MockService} from "ng-mocks";
import {ApiService} from "../api/api.service";

describe('CommissionsService', () => {
  let service: CommissionsService;

  beforeEach(() => {
    service = new CommissionsService(MockService(ApiService));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

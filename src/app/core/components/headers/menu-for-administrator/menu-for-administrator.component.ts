import { Component } from '@angular/core';

@Component({
  selector: 'app-menu-for-administrator',
  templateUrl: './menu-for-administrator.component.html',
  styleUrls: ['./menu-for-administrator.component.scss']
})
export class MenuForAdministratorComponent {}

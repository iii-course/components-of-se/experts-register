import { HeaderComponent } from './header.component';
import {AuthService} from "../../../services/auth/auth.service";
import {MockService} from "ng-mocks";
import { of } from 'rxjs';

describe('HeaderComponent', () => {
  let component: HeaderComponent;

  let authService: jasmine.SpyObj<AuthService>;

  beforeEach(() => {
    authService = MockService(AuthService, {
      userRole$: of({} as any),
      logout: jasmine.createSpy()
    }) as jasmine.SpyObj<AuthService>;

    component = new HeaderComponent(authService);
    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('logout', () => {
    it('should call auth service', () => {
      component.logout();
      expect(authService.logout).toHaveBeenCalled();
    });
  });
});

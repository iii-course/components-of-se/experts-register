import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserRole, UserRolesNames } from 'src/app/core/models/user-role.model';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public userRole$!: Observable<UserRole | undefined>;
  public UserRolesNames = UserRolesNames;

  constructor(
    private authService: AuthService
  ) {}

  public ngOnInit(): void {
    this.userRole$ = this.authService.userRole$;
  }

  public logout(): void {
    this.authService.logout();
  }
}

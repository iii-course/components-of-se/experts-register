export interface PassportData {
  number:	string,
  series: string,
  dateOfIssue: string,
  authorityCode: string,
}

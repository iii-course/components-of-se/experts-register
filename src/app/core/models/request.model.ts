import {Certificate} from "./certificate.model";
import {Expert} from "./expert.model";
import {Commission, CommissionDecision} from "./commission.model";
import {User} from "./user.model";

export enum RequestOperationTypes {
  ADDING_DATA = 'Включення даних',
  MODIFICATION = 'Унесення змін',
}

export interface RequestOperationType {
  id: number,
  operationType: string
}

export interface RequestStatus {
  id: number,
  status: string
}

export interface RequestPostData {
  operationTypeId: number,
  certificate: {
    id?: number,
    documentNumber?: number,
    validityEndDate?: string,
    expertiseTypeId?: number,
    expertiseSpecialitiesId?: number[]
  },
  expert: {
    id?: number,
    firstName?: string,
    lastName?: string,
    patronymic?: string,
    contacts?: {
      email?: string,
      phone: string
    },
    organisation?: {
      name: string,
      regionId: number,
      city: string,
      street: string,
      numberOfBuilding: number
    }
  },
  commission: {
    name: string,
    authorityId: number
  },
  commissionDecision: {
    number: string,
    date: string,
    typeId?: string
  },
  dismissalOrder?: {
    date: string,
    number: string
  }
}

export interface Request {
  id: number,
  date: string,
  comment?: string,
  operationType: RequestOperationType,
  status: RequestStatus,
  certificate: Certificate,
  expert: Expert,
  commission: Commission,
  commissionDecision: CommissionDecision,
  dismissalOrderDate?: string,
  dismissalOrderNumber?: number,
  author: User
}

export interface RequestsSearchTerm {
  authorId?: number,
  statusId?: number
}

import {Authority} from "./authority.model";

export interface Commission {
  name:	string
  authority:	Authority
}

export interface CommissionDecisionType {
  id: number,
  type: string
}

export interface CommissionDecision {
  number: number,
  date: string,
  type: CommissionDecisionType
}

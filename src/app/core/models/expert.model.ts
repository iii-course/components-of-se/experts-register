export interface Expert {
  id: number,
  firstName:	string,
  lastName:	string,
  patronymic:	string,
  contacts: {
    email:	string,
    phone:	string,
  },
  organisation: {
    name:	string,
    region:	string,
    city:	string,
    street:	string,
    numberOfBuilding: number,
  },
  authority: string,
  isFromGovernment:	boolean
}

export interface ExpertsSearchTerm {
    firstName?: string,
    lastName?: string,
    patronymic?: string,
    authorityId?: number,
    isFromGovernment?: boolean,
    expertiseClassId?: number[]
}

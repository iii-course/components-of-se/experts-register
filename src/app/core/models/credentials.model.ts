import { UserRole } from './user-role.model';

export interface Credentials {
    email: string;
    password: string;
    role: UserRole;
}
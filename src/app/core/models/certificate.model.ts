import {ExpertiseSpeciality, ExpertiseType} from "./expertise.model";
import {Commission, CommissionDecision} from "./commission.model";

export interface Certificate {
  id: number,
  documentNumber: number,
  validityEndDate: string,
  expertiseSpecialities: ExpertiseSpeciality[],
  expertiseType: ExpertiseType,
  commission: Commission,
  commissionDecision: CommissionDecision
}

export enum UserRolesNames {
    ADMIN = 'Адміністратор',
    APPLICANT_EXPERT = 'Заявник (Судовий експерт)',
    APPLICANT_HEAD = 'Заявник (Керівник державної установи або голова експертно-кваліфікаційної комісії)',
    APPLICANT_CENTRAL_COMMISSION_HEAD = 'Заявник (Голова Центральної експертно-кваліфікаційної комісії)',
    REGISTRAR = 'Реєстратор',
}

export interface UserRole {
    id: number,
    name: string, // @TODO: UserRolesNames
}

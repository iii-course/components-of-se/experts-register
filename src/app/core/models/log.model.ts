import {UserShortData} from "./user.model";

export interface TypeOfChange {
  id: number,
  name: string
}

export interface Log {
  id: number,
  author: UserShortData,
  affectedUser: UserShortData,
  date: string,
  typeOfChange: TypeOfChange,
  request?: {
    id: number,
    comment?: string,
    date: string,
    operationType: string
  }
}

export interface LogsSearchTerm {
  authorFirstName?: string,
  authorLastName?: string,
  authorPatronymic?: string,
  authorId?: number,
  affectedUserFirstName?: string,
  affectedUserLastName?: string,
  affectedUserPatronymic?: string,
  affectedUserId?: number,
  date?: string,
  typeOfChangeId?: number,
  requestId?: number,
  requestTypeId?: number,
  requestDate?: string
}

import {PassportData} from './passport-data.model';
import {UserRole} from './user-role.model';

export interface UserAuthData {
    id: number,
    jwt_token: string;
    role: UserRole
}

export interface User {
  id: number,
  role: UserRole,
  firstName: string,
  lastName: string,
  patronymic: string,
  birthdate: string,
  passport: PassportData,
  taxpayerRegistrationNumber: string,
  email: string,
  password: string,
  isActive: boolean,
  roleId?: number
}

export interface UserShortData {
  id: number,
  role: UserRole,
  firstName: string,
  lastName: string,
  patronymic: string,
  email: string,
}

export interface UsersSearchTerm {
  firstName?: string,
  lastName?: string,
  patronymic?: string,
  roleId?: number,
}

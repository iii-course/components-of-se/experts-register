export interface ExpertiseSpeciality {
  id:	number,
  number: number,
  name: string,
}

export interface ExpertiseType {
  id:	number,
  number: number,
  name: string,
  expertiseSpecialities: ExpertiseSpeciality[]
}

export interface ExpertiseClass {
  id:	number,
  classNumber: number,
  name: string,
  expertiseTypes: ExpertiseType[],
}

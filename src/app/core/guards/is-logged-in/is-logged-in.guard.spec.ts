import { IsLoggedInGuard } from './is-logged-in.guard';
import {MockService} from "ng-mocks";
import {AuthService} from "../../services/auth/auth.service";
import {Router} from "@angular/router";

describe('IsLoggedInGuard', () => {
  let guard: IsLoggedInGuard;

  beforeEach(() => {
    guard = new IsLoggedInGuard(MockService(AuthService), MockService(Router));
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

import { IsApplicantOrRegistrarGuard } from './is-applicant-or-registrar-guard.service';
import {MockService} from "ng-mocks";
import {AuthService} from "../../services/auth/auth.service";
import {Router} from "@angular/router";

describe('IsApplicantOrRegistrarGuard', () => {
  let guard: IsApplicantOrRegistrarGuard;

  beforeEach(() => {
    guard = new IsApplicantOrRegistrarGuard(MockService(AuthService), MockService(Router));
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { IsRegistrarGuard } from './is-registrar-guard.service';
import {MockService} from "ng-mocks";
import {AuthService} from "../../services/auth/auth.service";
import {Router} from "@angular/router";

describe('IsRegistrarGuard', () => {
  let guard: IsRegistrarGuard;

  beforeEach(() => {
    guard = new IsRegistrarGuard(MockService(AuthService), MockService(Router));
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

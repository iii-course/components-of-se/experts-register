import { TestBed } from '@angular/core/testing';

import { IsAdminGuard } from './is-admin.guard';
import {MockService} from "ng-mocks";
import {AuthService} from "../../services/auth/auth.service";
import {Router} from "@angular/router";

describe('IsAdminGuard', () => {
  let guard: IsAdminGuard;

  beforeEach(() => {
    guard = new IsAdminGuard(MockService(AuthService), MockService(Router));
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

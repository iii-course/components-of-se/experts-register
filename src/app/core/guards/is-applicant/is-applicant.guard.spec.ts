import { TestBed } from '@angular/core/testing';

import { IsApplicantGuard } from './is-applicant.guard';
import {MockService} from "ng-mocks";
import {AuthService} from "../../services/auth/auth.service";
import {Router} from "@angular/router";

describe('IsApplicantGuard', () => {
  let guard: IsApplicantGuard;

  beforeEach(() => {
    guard = new IsApplicantGuard(MockService(AuthService), MockService(Router));
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

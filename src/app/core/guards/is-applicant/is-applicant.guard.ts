import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from "../../services/auth/auth.service";
import {map} from "rxjs/operators";
import {UserRolesNames} from "../../models/user-role.model";

@Injectable({
  providedIn: 'root'
})
export class IsApplicantGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authService.userRole$.pipe(
      map((role) => {
        if (role.name !== UserRolesNames.APPLICANT_CENTRAL_COMMISSION_HEAD &&
          role.name !== UserRolesNames.APPLICANT_EXPERT &&
          role.name !== UserRolesNames.APPLICANT_HEAD) {
          return this.router.parseUrl('');
        }
        return true;
      })
    );
  }
}

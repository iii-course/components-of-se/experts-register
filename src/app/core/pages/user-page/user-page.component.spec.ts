import { UserPageComponent } from './user-page.component';
import { MockService} from "ng-mocks";
import {ActivatedRoute} from "@angular/router";
import {UsersService} from "../../services/users/users.service";

describe('UserPageComponent', () => {
  let component: UserPageComponent;

  beforeEach(() => {
    component = new UserPageComponent(
      MockService(ActivatedRoute),
      MockService(UsersService)
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

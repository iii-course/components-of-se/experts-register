import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {User} from "../../models/user.model";
import {Observable} from "rxjs";
import {UsersService} from "../../services/users/users.service";
import {getIdFromRoute} from "../../../helpers/router";

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {
  public user$!: Observable<User>;

  constructor(private route: ActivatedRoute, private usersService: UsersService) { }

  public ngOnInit(): void {
    const userId = getIdFromRoute(this.route);
    if (userId) {
      console.log(userId);
      this.user$ = this.usersService.getUserById(userId);
    }
  }
}

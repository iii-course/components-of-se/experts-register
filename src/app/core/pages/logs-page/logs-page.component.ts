import { Component } from '@angular/core';
import {Subject} from "rxjs";
import {LogsSearchTerm} from "../../models/log.model";

@Component({
  selector: 'app-logs-page',
  templateUrl: './logs-page.component.html',
  styleUrls: ['./logs-page.component.scss']
})
export class LogsPageComponent {
  private searchParametersSubject$ = new Subject<LogsSearchTerm>();

  public searchParameters$ = this.searchParametersSubject$.asObservable();

  public onSearch(params: LogsSearchTerm): void {
    this.searchParametersSubject$.next(params);
  }
}

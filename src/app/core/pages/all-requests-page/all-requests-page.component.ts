import { Component } from '@angular/core';
import {Subject} from "rxjs";
import {RequestsSearchTerm} from "../../models/request.model";

@Component({
  selector: 'app-all-requests-page',
  templateUrl: './all-requests-page.component.html',
  styleUrls: ['./all-requests-page.component.scss']
})
export class AllRequestsPageComponent {
  private searchParametersSubject$ = new Subject<RequestsSearchTerm>();

  public searchParameters$ = this.searchParametersSubject$.asObservable();

  public onSearch(params: RequestsSearchTerm): void {
    this.searchParametersSubject$.next(params);
  }
}

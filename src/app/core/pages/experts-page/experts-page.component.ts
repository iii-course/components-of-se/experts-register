import {Component, ElementRef, ViewChild} from '@angular/core';
import {Expert, ExpertsSearchTerm} from "../../models/expert.model";
import {Subject} from "rxjs";

@Component({
  selector: 'app-experts-page',
  templateUrl: './experts-page.component.html',
  styleUrls: ['./experts-page.component.scss']
})
export class ExpertsPageComponent {
  @ViewChild('expertCardComponent') set component(component: ElementRef) {
    if (component) {
      this.expertCardComponent = component.nativeElement as Element;
      this.expertCardComponent?.scrollIntoView({ behavior: "smooth", block: "start" });
    }
  }
  private expertSubject$ = new Subject<Expert>();
  private searchParametersSubject$ = new Subject<ExpertsSearchTerm>();
  private expertCardComponent!: Element;

  public expert$ = this.expertSubject$.asObservable();
  public searchParameters$ = this.searchParametersSubject$.asObservable();

  public onExpertSelected(expertSelected: Expert): void {
    this.expertSubject$.next(expertSelected);
    this.expertCardComponent?.scrollIntoView();
  }

  public onSearch(params: ExpertsSearchTerm): void {
    this.searchParametersSubject$.next(params);
  }
}

import { ExpertsPageComponent } from './experts-page.component';

describe('ExpertsPageComponent', () => {
  let component: ExpertsPageComponent;

  beforeEach(() => {
    component = new ExpertsPageComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should scroll to the card if a card has been selected', () => {
    const scrollIntoView = jasmine.createSpy();
    component.component = {nativeElement: {scrollIntoView}};

    expect(scrollIntoView).toHaveBeenCalled();
  });
});

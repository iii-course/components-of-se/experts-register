import { RequestsQueuePageComponent } from './requests-queue-page.component';
import {MockService} from "ng-mocks";
import {RequestsService} from "../../services/requests/requests.service";

describe('RequestsQueuePageComponent', () => {
  let component: RequestsQueuePageComponent;

  beforeEach(() => {
    component = new RequestsQueuePageComponent(MockService(RequestsService));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

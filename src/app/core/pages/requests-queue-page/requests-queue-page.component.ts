import {Component, OnInit} from '@angular/core';
import {RequestsSearchTerm} from "../../models/request.model";
import {RequestsService} from "../../services/requests/requests.service";
import {RequestsStatuses} from "../../../../mock/requests.mock";

@Component({
  selector: 'app-requests-queue-page',
  templateUrl: './requests-queue-page.component.html',
  styleUrls: ['./requests-queue-page.component.scss']
})
export class RequestsQueuePageComponent implements OnInit {
  public params!: RequestsSearchTerm;

  constructor(private requestsService: RequestsService) {}

  public ngOnInit(): void {
    this.requestsService.getStatusId(RequestsStatuses.WAITING_FOR_APPROVAL)
      .subscribe(statusId => this.params = {statusId});
  }
}

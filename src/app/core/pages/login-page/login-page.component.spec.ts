import { LoginPageComponent } from './login-page.component';
import {FormBuilder} from "@angular/forms";
import {AuthService} from "../../services/auth/auth.service";
import {MockService} from "ng-mocks";
import {of, throwError} from 'rxjs';
import {AlertService} from "../../services/alert/alert.service";

describe('LoginPageComponent', () => {
  let component: LoginPageComponent;

  let authService: jasmine.SpyObj<AuthService>;
  let alertService: jasmine.SpyObj<AlertService>;

  beforeEach(() => {

    authService = MockService(AuthService, {
      login: jasmine.createSpy().and.returnValue(of(undefined))
    }) as jasmine.SpyObj<AuthService>;

    alertService = MockService(AlertService, {
      alertInvalidCredentialsError: jasmine.createSpy()
    }) as jasmine.SpyObj<AlertService>;

    component = new LoginPageComponent(
      new FormBuilder(),
      authService,
      alertService
    );
    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onSubmit', () => {
    it('should call auth service', () => {
      component.onSubmit();
      expect(authService.login).toHaveBeenCalled();
    });

    it('should alert in case of an error', () => {
      authService.login.and.returnValue(throwError(new Error()));
      component.onSubmit();

      expect(alertService.alertInvalidCredentialsError).toHaveBeenCalled();
    });
  });
});

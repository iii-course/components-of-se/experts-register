import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {EMPTY, Subscription} from 'rxjs';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import {catchError} from "rxjs/operators";
import {TranslocoService} from "@ngneat/transloco";
import {AlertService} from "../../services/alert/alert.service";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit, OnDestroy {
  public loginFormGroup!: FormGroup;
  private subscription = new Subscription();

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private alertService: AlertService
  ) { }

  public ngOnInit(): void {
    this.setupFormGroup();
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public onSubmit(): void {
    this.subscription.add(
      this.authService.login(this.loginFormGroup.value).pipe(
        catchError(() => {
          this.alertService.alertInvalidCredentialsError();
          return EMPTY;
        })
      ).subscribe()
    );
  }

  private setupFormGroup() {
    this.loginFormGroup = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
}

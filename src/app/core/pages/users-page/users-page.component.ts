import { Component } from '@angular/core';
import {Subject} from "rxjs";
import {UsersSearchTerm} from "../../models/user.model";

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.scss']
})
export class UsersPageComponent {
  private searchParametersSubject$ = new Subject<UsersSearchTerm>();

  public searchParameters$ = this.searchParametersSubject$.asObservable();

  public onSearch(params: UsersSearchTerm): void {
    this.searchParametersSubject$.next(params);
  }
}

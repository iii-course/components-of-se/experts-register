import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {AuthService} from "../../services/auth/auth.service";
import {RequestsService} from "../../services/requests/requests.service";
import {concatMap, map, switchMap, take} from "rxjs/operators";
import {RequestsStatuses} from "../../../../mock/requests.mock";
import {Request} from "../../models/request.model";

@Component({
  selector: 'app-applicants-requests-page',
  templateUrl: './applicants-requests-page.component.html',
  styleUrls: ['./applicants-requests-page.component.scss']
})
export class ApplicantsRequestsPageComponent implements OnInit {
  public approvedRequests$!: Observable<Request[]>;
  public rejectedRequests$!: Observable<Request[]>;
  public waitingRequests$!: Observable<Request[]>;

  constructor(
    private authService: AuthService,
    private requestsService: RequestsService
  ) { }

  public ngOnInit(): void {
    this.approvedRequests$ = this.getRequestsWithStatus$(RequestsStatuses.APPROVED);
    this.rejectedRequests$ = this.getRequestsWithStatus$(RequestsStatuses.REJECTED);
    this.waitingRequests$ = this.getRequestsWithStatus$(RequestsStatuses.WAITING_FOR_APPROVAL);
  }

  private getRequestsWithStatus$(statusName: string): Observable<Request[]> {
    return this.requestsService.getStatusId(statusName).pipe(
      take(1),
      map(statusId => statusId ? ({
        authorId: this.authService.user?.id,
        statusId
      }) : { authorId: this.authService.user?.id }),
      concatMap(params => this.requestsService.getAllRequests(params))
    );
  }
}

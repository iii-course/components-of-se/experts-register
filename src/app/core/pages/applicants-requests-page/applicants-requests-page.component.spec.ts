import { ApplicantsRequestsPageComponent } from './applicants-requests-page.component';
import {AuthService} from "../../services/auth/auth.service";
import {RequestsService} from "../../services/requests/requests.service";
import {MockService} from "ng-mocks";
import { of } from 'rxjs';

describe('ApplicantsRequestsPageComponent', () => {
  let component: ApplicantsRequestsPageComponent;

  let authService: jasmine.SpyObj<AuthService>;
  let requestsService: jasmine.SpyObj<RequestsService>;

  const userIdMock = 1;
  const statusIdMock = 1;
  let requestsMock: Request[];

  beforeEach(() => {
    requestsMock = [{id: 1} as any];

    authService = MockService(AuthService, {
      userId$: of(userIdMock)
    }) as jasmine.SpyObj<AuthService>;
    requestsService = MockService(RequestsService, {
      getAllRequests: jasmine.createSpy().and.returnValue(of(requestsMock)),
      getStatusId: jasmine.createSpy().and.returnValue(statusIdMock),
    }) as jasmine.SpyObj<RequestsService>;

    component = new ApplicantsRequestsPageComponent(
      authService,
      requestsService
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('approvedRequests$', () => {
    it('should get requests', () => {
      const spy = jasmine.createSpy();
      component.approvedRequests$.subscribe(spy);

      expect(spy).toHaveBeenCalledOnceWith(requestsMock);
    });

    it('should use correct params', () => {
      component.approvedRequests$.subscribe();
      const params = {authorId: userIdMock, statusId: statusIdMock};

      expect(requestsService.getAllRequests).toHaveBeenCalledOnceWith(jasmine.objectContaining(params));
    });
  });

  describe('rejectedRequests$', () => {
    it('should get requests', () => {
      const spy = jasmine.createSpy();
      component.rejectedRequests$.subscribe(spy);

      expect(spy).toHaveBeenCalledOnceWith(requestsMock);
    });

    it('should use correct params', () => {
      component.rejectedRequests$.subscribe();
      const params = {authorId: userIdMock, statusId: statusIdMock};

      expect(requestsService.getAllRequests).toHaveBeenCalledOnceWith(jasmine.objectContaining(params));
    });
  });

  describe('waitingRequests$', () => {
    it('should get requests', () => {
      const spy = jasmine.createSpy();
      component.waitingRequests$.subscribe(spy);

      expect(spy).toHaveBeenCalledOnceWith(requestsMock);
    });

    it('should use correct params', () => {
      component.waitingRequests$.subscribe();
      const params = {authorId: userIdMock, statusId: statusIdMock};

      expect(requestsService.getAllRequests).toHaveBeenCalledOnceWith(jasmine.objectContaining(params));
    });
  });
});

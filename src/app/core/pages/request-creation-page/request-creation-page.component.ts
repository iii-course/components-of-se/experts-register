import { Component } from '@angular/core';

@Component({
  selector: 'app-request-creation-page',
  templateUrl: './request-creation-page.component.html',
  styleUrls: ['./request-creation-page.component.scss']
})
export class RequestCreationPageComponent {}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestCreationPageComponent } from './request-creation-page.component';

describe('RequestCreationPageComponent', () => {
  let component: RequestCreationPageComponent;
  let fixture: ComponentFixture<RequestCreationPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestCreationPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestCreationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

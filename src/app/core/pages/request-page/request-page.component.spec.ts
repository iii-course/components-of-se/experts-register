import { RequestPageComponent } from './request-page.component';
import { MockService} from "ng-mocks";
import {ActivatedRoute} from "@angular/router";
import {RequestsService} from "../../services/requests/requests.service";

describe('RequestPageComponent', () => {
  let component: RequestPageComponent;

  beforeEach(() => {
    component = new RequestPageComponent(MockService(ActivatedRoute), MockService(RequestsService));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

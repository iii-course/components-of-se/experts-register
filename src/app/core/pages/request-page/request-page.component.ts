import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {Request} from "../../models/request.model";
import {getIdFromRoute} from "../../../helpers/router";
import {RequestsService} from "../../services/requests/requests.service";

@Component({
  selector: 'app-request-page',
  templateUrl: './request-page.component.html',
  styleUrls: ['./request-page.component.scss']
})
export class RequestPageComponent implements OnInit {
  public request$!: Observable<Request>;

  constructor(private route: ActivatedRoute, private requestsService: RequestsService) { }

  public ngOnInit(): void {
    this.fetchRequestById();
  }

  private fetchRequestById(): void {
    const requestId = getIdFromRoute(this.route);
    if (requestId) {
      this.request$ = this.requestsService.getRequestById(requestId);
    }
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExpertsPageComponent } from './core/pages/experts-page/experts-page.component';
import { LoginPageComponent } from './core/pages/login-page/login-page.component';
import {UsersPageComponent} from "./core/pages/users-page/users-page.component";
import {UserPageComponent} from "./core/pages/user-page/user-page.component";
import {RegistrationPageComponent} from "./core/pages/registration-page/registration-page.component";
import {LogsPageComponent} from "./core/pages/logs-page/logs-page.component";
import {ApplicantsRequestsPageComponent} from "./core/pages/applicants-requests-page/applicants-requests-page.component";
import {RequestPageComponent} from "./core/pages/request-page/request-page.component";
import {RequestCreationPageComponent} from "./core/pages/request-creation-page/request-creation-page.component";
import {AllRequestsPageComponent} from "./core/pages/all-requests-page/all-requests-page.component";
import {RequestsQueuePageComponent} from "./core/pages/requests-queue-page/requests-queue-page.component";
import {IsLoggedInGuard} from "./core/guards/is-logged-in/is-logged-in.guard";
import {IsAdminGuard} from "./core/guards/is-admin/is-admin.guard";
import {IsApplicantGuard} from "./core/guards/is-applicant/is-applicant.guard";
import {IsRegistrarGuard} from "./core/guards/is-registrar/is-registrar-guard.service";
import {IsApplicantOrRegistrarGuard} from "./core/guards/is-applicant-or-registrar/is-applicant-or-registrar-guard.service";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'experts'
  },
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: 'experts',
    component: ExpertsPageComponent,
    canActivate: [IsLoggedInGuard]
  },
  {
    path: 'users',
    pathMatch: 'full',
    component: UsersPageComponent,
    canActivate: [IsLoggedInGuard, IsAdminGuard]
  },
  {
    path: 'users/:id',
    component: UserPageComponent,
    canActivate: [IsLoggedInGuard, IsAdminGuard]
  },
  {
    path: 'registration',
    component: RegistrationPageComponent,
    canActivate: [IsLoggedInGuard, IsAdminGuard]
  },
  {
    path: 'logs',
    component: LogsPageComponent,
    canActivate: [IsLoggedInGuard, IsAdminGuard]
  },
  {
    path: 'applicants-requests',
    component: ApplicantsRequestsPageComponent,
    canActivate: [IsLoggedInGuard, IsApplicantGuard]
  },
  {
    path: 'all-requests',
    component: AllRequestsPageComponent,
    canActivate: [IsLoggedInGuard, IsRegistrarGuard]
  },
  {
    path: 'requests-queue',
    component: RequestsQueuePageComponent,
    canActivate: [IsLoggedInGuard, IsRegistrarGuard]
  },
  {
    path: 'requests/creation',
    pathMatch: 'full',
    component: RequestCreationPageComponent,
    canActivate: [IsLoggedInGuard, IsApplicantGuard]
  },
  {
    path: 'requests/:id',
    component: RequestPageComponent,
    canActivate: [IsLoggedInGuard, IsApplicantOrRegistrarGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

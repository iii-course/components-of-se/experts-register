import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './core/services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public title = 'experts-register';
  public isUserLoggedIn$!: Observable<boolean>;

  constructor(private authService: AuthService) {}

  public ngOnInit(): void {
    this.isUserLoggedIn$ = this.authService.isUserLoggedIn$;
  }
}

import {ValidatorFn, Validators} from "@angular/forms";

export const stringValidators: ValidatorFn[] = [
  Validators.minLength(2),
  Validators.maxLength(100),
 // Validators.pattern(/^[a-zA-Zа-яА-Я -']*$/)
];

export const passwordValidators: ValidatorFn[] = [
  Validators.minLength(8),
  Validators.maxLength(100),
];

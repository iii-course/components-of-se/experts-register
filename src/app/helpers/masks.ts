import IMask from "imask";
import AnyMaskedOptions = IMask.AnyMaskedOptions;

export const stringMask: AnyMaskedOptions = {
  mask: String
};

export const phoneMask: AnyMaskedOptions = {
  mask: '{380}000000000'
};

export const dateMask: AnyMaskedOptions = {
  mask: Date,
  min: new Date(1900, 0, 1),
  max: new Date(),
};

export const possiblyFutureDateMask: AnyMaskedOptions = {
  mask: Date,
  min: new Date(1900, 0, 1),
  max: new Date(2100, 0, 1),
};

export const passportNumberMask: AnyMaskedOptions = {
  mask: '000000000'
};

export const passportSeriesMask: AnyMaskedOptions = {
  mask: '00'
};

export const passportAuthorityCodeMask: AnyMaskedOptions = {
  mask: '0000'
};

export const taxpayerNumberMask: AnyMaskedOptions = {
  mask: '0000000000'
};

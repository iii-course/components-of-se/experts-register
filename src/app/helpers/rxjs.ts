export function isDefined<T>(arg: T | null | undefined): arg is T {
  // https://stackoverflow.com/questions/57999777/filter-undefined-from-rxjs-observable
  return arg !== null && arg !== undefined;
}

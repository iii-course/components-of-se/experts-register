import {ActivatedRoute} from "@angular/router";

export function getIdFromRoute(route: ActivatedRoute): number | undefined {
  try {
    return Number.parseInt(route.snapshot.params['id']);
  } catch (_) {
    return undefined;
  }
}

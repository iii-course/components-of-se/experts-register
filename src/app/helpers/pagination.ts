import {PageEvent} from "@angular/material/paginator";

export interface PaginationParameters {
  offset: number,
  limit: number
}

export function calculatePaginationParameters(event: PageEvent): PaginationParameters {
  const {pageIndex, pageSize} = event;
  const offset = pageIndex * pageSize;
  const limit = pageSize;
  return {offset, limit};
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatExpansionModule} from '@angular/material/expansion';
import { TranslocoRootModule } from '../transloco-root.module';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { LoaderComponent } from './components/loader/loader.component';
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from "@angular/material/dialog";

const SHARED_MODULES = [
  CommonModule,
  TranslocoRootModule,
  ReactiveFormsModule,
  MatFormFieldModule,
  MatButtonModule,
  MatRadioModule,
  MatSelectModule,
  MatInputModule,
  MatToolbarModule,
  MatExpansionModule,
  MatCheckboxModule,
  MatTableModule,
  MatPaginatorModule,
  MatCardModule,
  MatDialogModule
];

@NgModule({
  declarations: [
    LoaderComponent
  ],
  imports: [
    ...SHARED_MODULES
  ],
  exports: [
    ...SHARED_MODULES,
    LoaderComponent
  ]
})
export class SharedModule { }

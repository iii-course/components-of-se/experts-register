import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './core/pages/login-page/login-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { HeaderComponent } from './core/components/headers/header/header.component';
import { MenuForAdministratorComponent } from './core/components/headers/menu-for-administrator/menu-for-administrator.component';
import { MenuForRegistarComponent } from './core/components/headers/menu-for-registar/menu-for-registar.component';
import { MenuForApplicantComponent } from './core/components/headers/menu-for-applicant/menu-for-applicant.component';
import { CommonModule } from '@angular/common';
import { ExpertsPageComponent } from './core/pages/experts-page/experts-page.component';
import { ExpertsModule } from './features/experts/experts.module';
import { RouterModule } from '@angular/router';
import { UsersPageComponent } from './core/pages/users-page/users-page.component';
import {UsersModule} from "./features/users/users.module";
import { UserPageComponent } from './core/pages/user-page/user-page.component';
import { RegistrationPageComponent } from './core/pages/registration-page/registration-page.component';
import { LogsPageComponent } from './core/pages/logs-page/logs-page.component';
import {LogsModule} from "./features/logs/logs.module";
import { ApplicantsRequestsPageComponent } from './core/pages/applicants-requests-page/applicants-requests-page.component';
import {RequestsModule} from "./features/requests/requests.module";
import { RequestPageComponent } from './core/pages/request-page/request-page.component';
import { RequestCreationPageComponent } from './core/pages/request-creation-page/request-creation-page.component';
import { AllRequestsPageComponent } from './core/pages/all-requests-page/all-requests-page.component';
import { RequestsQueuePageComponent } from './core/pages/requests-queue-page/requests-queue-page.component';
import {httpInterceptorProviders} from "./core/interceptors/http.interceptors";


@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    HeaderComponent,
    MenuForAdministratorComponent,
    MenuForRegistarComponent,
    MenuForApplicantComponent,
    ExpertsPageComponent,
    UsersPageComponent,
    UserPageComponent,
    RegistrationPageComponent,
    LogsPageComponent,
    ApplicantsRequestsPageComponent,
    RequestPageComponent,
    RequestCreationPageComponent,
    AllRequestsPageComponent,
    RequestsQueuePageComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    ExpertsModule,
    RouterModule,
    UsersModule,
    LogsModule,
    RequestsModule
  ],
  providers: [
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

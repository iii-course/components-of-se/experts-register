import { ExpertsSearchFormComponent } from './experts-search-form.component';
import {AuthoritiesService} from "../../../../core/services/authorities/authorities.service";
import {ExpertiseService} from "../../../../core/services/expertise/expertise.service";
import {FormBuilder} from "@angular/forms";
import {MockService} from "ng-mocks";

describe('ExpertsSearchFormComponent', () => {
  let component: ExpertsSearchFormComponent;

  let authoritiesService: AuthoritiesService;
  let expertiseService: ExpertiseService;

  beforeEach(() => {
    authoritiesService = MockService(AuthoritiesService);
    expertiseService = MockService(ExpertiseService);

    component = new ExpertsSearchFormComponent(
      new FormBuilder(),
      authoritiesService,
      expertiseService
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

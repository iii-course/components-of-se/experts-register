import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Authority} from "../../../../core/models/authority.model";
import {AuthoritiesService} from "../../../../core/services/authorities/authorities.service";
import {Observable, Subscription} from "rxjs";
import {ExpertiseClass} from "../../../../core/models/expertise.model";
import {ExpertiseService} from "../../../../core/services/expertise/expertise.service";
import {ExpertsSearchTerm} from "../../../../core/models/expert.model";
import {stringValidators} from "../../../../helpers/validators";
import {UsersSearchTerm} from "../../../../core/models/user.model";

@Component({
  selector: 'app-experts-search-form',
  templateUrl: './experts-search-form.component.html',
  styleUrls: ['./experts-search-form.component.scss']
})
export class ExpertsSearchFormComponent implements OnInit, OnDestroy {
  @Output() searchTerm = new EventEmitter<ExpertsSearchTerm>();
  public searchFormGroup!: FormGroup;
  public authorities$!: Observable<Authority[]>;
  public expertiseClasses!: ExpertiseClass[];

  private expertiseClasses$!: Observable<ExpertiseClass[]>;
  private subscription = new Subscription();

  constructor(
    private fb: FormBuilder,
    private authoritiesService: AuthoritiesService,
    private expertiseService: ExpertiseService
  ) {}

  public ngOnInit(): void {
    this.fetchAllData();
    this.setUpForm();
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public onSubmit(): void {
    this.searchTerm.emit(this.getSearchTerm());
  }

  private getSearchTerm(): ExpertsSearchTerm {
      const searchTerm = new Map();
      Object.entries(this.searchFormGroup.value)
        .filter(([_, value]) => !!value)
        .forEach(([key, value]) => searchTerm.set(key, value));
      const searchTermFull = {
        ...Object.fromEntries(searchTerm), expertiseClassId: this.getSelectedExpertiseClassIds()
      };
      return searchTermFull as ExpertsSearchTerm;
  }

  private setUpForm() {
    this.searchFormGroup = this.fb.group({
      firstName: ['', stringValidators],
      lastName: ['', stringValidators],
      patronymic: ['', stringValidators],
      authorityId: null,
      isFromGovernment: null,
      expertiseClassId: this.fb.group({})
    });

    this.setUpExpertiseClassIdFormGroup();
  }

  private fetchAllData() {
    this.fetchAuthorities();
    this.fetchExpertiseClasses();
  }

  private fetchAuthorities(): void {
    this.authorities$ = this.authoritiesService.getAllAuthorities();
  }

  private fetchExpertiseClasses(): void {
    this.expertiseClasses$ = this.expertiseService.getAllExpertiseClasses();
  }

  private getSelectedExpertiseClassIds(): number[] {
    return Object.entries(this.expertiseClassIdFormGroup.value)
      .filter(([_, isSelected]) => isSelected)
      .map(([classId]) => Number.parseInt(classId));
  }

  private setUpExpertiseClassIdFormGroup(): void {
    this.subscription.add(
      this.expertiseClasses$.subscribe((classes) => {
        this.expertiseClasses = classes;
        for (const expertiseClass of classes) {
          this.expertiseClassIdFormGroup.addControl(expertiseClass.id.toString(), this.fb.control(false));
        }
      })
    );
  }

  private get expertiseClassIdFormGroup(): FormGroup {
    return this.searchFormGroup?.controls['expertiseClassId'] as FormGroup;
  }
}

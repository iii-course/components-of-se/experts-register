import {Component, Input, OnInit} from '@angular/core';
import {Expert} from "../../../../core/models/expert.model";
import {ExpertsService} from "../../../../core/services/experts/experts.service";
import {Observable} from "rxjs";
import {Certificate} from "../../../../core/models/certificate.model";
import {TranslocoService} from "@ngneat/transloco";

@Component({
  selector: 'app-expert-card',
  templateUrl: './expert-card.component.html',
  styleUrls: ['./expert-card.component.scss']
})
export class ExpertCardComponent implements OnInit {
  @Input() expert!: Expert;
  public certificates$!: Observable<Certificate[]>;
  public certificatesColumns = ['certificate', 'commission', 'commissionDecision', 'expertiseType', 'expertiseSpecialities'];

  constructor(private expertsService: ExpertsService, private translocoService: TranslocoService) {}

  public ngOnInit(): void {
    if (this.expert) {
      this.certificates$ = this.expertsService.getExpertCertificates(this.expert.id);
    }
  }

  public downloadExtract(): void {
    alert(this.translocoService.translate<string>('this_function_is_not_available'));
  }
}

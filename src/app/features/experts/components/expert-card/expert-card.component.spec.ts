import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertCardComponent } from './expert-card.component';
import {ExpertsService} from "../../../../core/services/experts/experts.service";
import {MockService} from "ng-mocks";
import {TranslocoService} from "@ngneat/transloco";

describe('ExpertCardComponent', () => {
  let component: ExpertCardComponent;

  let expertsService: jasmine.SpyObj<ExpertsService>;

  beforeEach(() => {
    expertsService = MockService(ExpertsService) as jasmine.SpyObj<ExpertsService>;
    component = new ExpertCardComponent(expertsService, MockService(TranslocoService));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

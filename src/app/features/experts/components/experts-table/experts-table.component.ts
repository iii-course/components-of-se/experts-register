import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {ExpertsService} from "../../../../core/services/experts/experts.service";
import {Observable, of} from "rxjs";
import {Expert, ExpertsSearchTerm} from "../../../../core/models/expert.model";
import {PageEvent} from "@angular/material/paginator";
import {calculatePaginationParameters, PaginationParameters} from "../../../../helpers/pagination";

@Component({
  selector: 'app-experts-table',
  templateUrl: './experts-table.component.html',
  styleUrls: ['./experts-table.component.scss']
})
export class ExpertsTableComponent implements OnInit, OnChanges {
  @Output() expertSelected = new EventEmitter<Expert>();
  @Input() parameters!: ExpertsSearchTerm | null;
  public experts$: Observable<Expert[] | undefined> = of(undefined);
  public expertTotalCount$: Observable<number | undefined> = of(undefined);
  public columnsToDisplay = ['lastName', 'firstName', 'patronymic', 'isFromGovernment', 'authority', 'action'];

  private paginationParams: PaginationParameters = {
    limit: 10,
    offset: 0
  };

  constructor(private expertsService: ExpertsService) { }

  public ngOnInit(): void {
    this.updateExperts();
  }

  public ngOnChanges(): void {
    this.updateExperts();
  }

  public onPageSelected(event: PageEvent): void {
    this.paginationParams = calculatePaginationParameters(event);
    const params = {...this.parameters, ...this.paginationParams};
    this.experts$ = this.expertsService.getAllExperts(params);
  }

  public onExpertSelected(expert: Expert): void {
    this.expertSelected.emit(expert);
  }

  private updateExperts(): void {
    const params = {...this.parameters, ...this.paginationParams};
    this.experts$ = this.expertsService.getAllExperts(params);
    this.expertTotalCount$ = this.expertsService.getExpertsTotalCount(params);
  }
}

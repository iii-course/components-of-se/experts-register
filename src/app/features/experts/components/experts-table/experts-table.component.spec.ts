import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertsTableComponent } from './experts-table.component';
import {ExpertsService} from "../../../../core/services/experts/experts.service";
import {MockService} from "ng-mocks";

describe('ExpertsTableComponent', () => {
  let component: ExpertsTableComponent;

  let expertsService: jasmine.SpyObj<ExpertsService>;

  beforeEach(() => {
    expertsService = MockService(ExpertsService) as jasmine.SpyObj<ExpertsService>;

    component = new ExpertsTableComponent(expertsService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

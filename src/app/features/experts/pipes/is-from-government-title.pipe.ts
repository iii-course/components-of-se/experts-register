import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isFromGovernmentTitle'
})
export class IsFromGovernmentTitlePipe implements PipeTransform {

  public transform(value: boolean): string {
    return value ? 'expert_from_government' : 'expert_not_from_government';
  }

}

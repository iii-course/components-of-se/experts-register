import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExpertsSearchFormComponent } from './components/experts-search-form/experts-search-form.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { IsFromGovernmentTitlePipe } from './pipes/is-from-government-title.pipe';
import { ExpertsTableComponent } from './components/experts-table/experts-table.component';
import { ExpertCardComponent } from './components/expert-card/expert-card.component';

@NgModule({
  declarations: [
    ExpertsSearchFormComponent,
    IsFromGovernmentTitlePipe,
    ExpertsTableComponent,
    ExpertCardComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    ExpertsSearchFormComponent,
    ExpertsTableComponent,
    ExpertCardComponent
  ]
})
export class ExpertsModule { }

import {Validators} from "@angular/forms";
import {stringValidators} from "../../../helpers/validators";

export const certificateFormGroup = (id: number | undefined,
                                     documentNumber: number | undefined,
                                     validityEndDate: string | undefined,
                                     expertiseClassId: number | undefined,
                                     expertiseTypeId: number | undefined,
                                     expertiseSpecialitiesId: number[] | undefined) => ({
  id: [id],
  documentNumber: [documentNumber, [Validators.min(1), Validators.max(1000000)]],
  validityEndDate: [validityEndDate],
  expertiseClassId: [expertiseClassId],
  expertiseTypeId: [expertiseTypeId],
  expertiseSpecialitiesId: [expertiseSpecialitiesId],
});

export const dismissalOrderFormGroup = (number: number | undefined, date: string | undefined) => ({
  number: [number, [Validators.min(1), Validators.max(1000000)]],
  date: [date]
});

export const expertFormGroup = (id: number | undefined, firstName: string | undefined, lastName: string | undefined,
                                patronymic: string | undefined, disabled?: boolean) => ({
  id: [id],
  firstName: [firstName, stringValidators],
  lastName: [lastName, stringValidators],
  patronymic: [patronymic, stringValidators]
});

export const commissionFormGroup = (name: string | undefined, authorityId: number | undefined) => ({
  name: [name, [Validators.required, ...stringValidators]],
  authorityId: [authorityId, Validators.required],
});

export const commissionDecisionFormGroup = (number: number | undefined,
                                            date: string | undefined,
                                            typeId: number | undefined) => ({
  number: [number, [Validators.required, Validators.min(1), Validators.max(1000000)]],
  date: [date, Validators.required],
  typeId: [typeId],
});

export const contactsFormGroup = (email: string | undefined, phone: string | undefined) => ({
  email: [email, Validators.email],
  phone: [phone, Validators.required]
});

export const organisationFormGroup = () => ({
  name: [undefined, [Validators.required, ...stringValidators]],
  regionId: [undefined, Validators.required],
  city: [undefined, [Validators.required, ...stringValidators]],
  street: [undefined, [Validators.required, ...stringValidators]],
  numberOfBuilding: [undefined, [Validators.required, Validators.min(1), Validators.max(1000)]]
});

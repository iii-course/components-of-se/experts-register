import { RequestsTableComponent } from './requests-table.component';
import {MockService} from "ng-mocks";
import {RequestsService} from "../../../../../core/services/requests/requests.service";
import {Router} from "@angular/router";

describe('RequestsTableComponent', () => {
  let component: RequestsTableComponent;

  beforeEach(() => {
    component = new RequestsTableComponent(
      MockService(RequestsService),
      MockService(Router)
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Request, RequestsSearchTerm} from "../../../../../core/models/request.model";
import {RequestsService} from "../../../../../core/services/requests/requests.service";
import {Observable} from "rxjs";
import {Router} from "@angular/router";
import {PageEvent} from "@angular/material/paginator";
import {calculatePaginationParameters, PaginationParameters} from "../../../../../helpers/pagination";


@Component({
  selector: 'app-requests-table',
  templateUrl: './requests-table.component.html',
  styleUrls: ['./requests-table.component.scss']
})
export class RequestsTableComponent implements OnInit, OnChanges {
  @Input() searchTerm: RequestsSearchTerm | null = null;

  public requests$!: Observable<Request[]>;
  public requestsTotalCount$!: Observable<number>;

  public columnsToDisplay = ['id', 'author', 'expert', 'operationType', 'status', 'date', 'action'];

  private paginationParams: PaginationParameters = {
    limit: 10,
    offset: 0
  };

  constructor(private requestsService: RequestsService, private router: Router) { }

  public ngOnInit(): void {
    this.updateRequests();
  }

  public ngOnChanges(): void {
    this.updateRequests();
  }

  public onPageSelected(event: PageEvent): void {
    this.paginationParams = calculatePaginationParameters(event);
    const params = {...this.searchTerm, ...this.paginationParams};
    this.requests$ = this.requestsService.getAllRequests(params);
  }

  private updateRequests(): void {
    const params = {...this.searchTerm, ...this.paginationParams};
    this.requests$ = this.requestsService.getAllRequests(params);
    this.requestsTotalCount$ = this.requestsService.getRequestsCount(params);
  }

  public onRequestSelected(request: Request): void {
    void this.router.navigateByUrl(`requests/${request.id}`);
  }

}

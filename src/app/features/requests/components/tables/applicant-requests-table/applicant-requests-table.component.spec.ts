import { ApplicantRequestsTableComponent } from './applicant-requests-table.component';
import {MockService} from "ng-mocks";
import {Router} from "@angular/router";

describe('ApplicantRequestsTableComponent', () => {
  let component: ApplicantRequestsTableComponent;

  beforeEach(() => {
    component = new ApplicantRequestsTableComponent(MockService(Router));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input } from '@angular/core';
import {Observable} from "rxjs";
import {Request} from "../../../../../core/models/request.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-applicant-requests-table',
  templateUrl: './applicant-requests-table.component.html',
  styleUrls: ['./applicant-requests-table.component.scss']
})
export class ApplicantRequestsTableComponent {
  @Input() requests$!: Observable<Request[]>;

  public columnsToDisplay = ['id', 'expert', 'operationType', 'date', 'action'];

  constructor(private router: Router) {}

  public onRequestSelected(request: Request): void {
    void this.router.navigateByUrl(`requests/${request.id}`);
  }
}

import {Component, Input, OnInit} from '@angular/core';
import {Request, RequestOperationType, RequestOperationTypes} from "../../../../../core/models/request.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BehaviorSubject, EMPTY, Observable, of, Subscription} from "rxjs";
import {UsersService} from "../../../../../core/services/users/users.service";
import {AlertService} from "../../../../../core/services/alert/alert.service";
import {dateMask, phoneMask, possiblyFutureDateMask} from "../../../../../helpers/masks";
import {
  certificateFormGroup,
  commissionDecisionFormGroup,
  commissionFormGroup,
  contactsFormGroup, dismissalOrderFormGroup,
  expertFormGroup, organisationFormGroup
} from "../../../helpers/form-groups";
import {Authority} from "../../../../../core/models/authority.model";
import {AuthoritiesService} from "../../../../../core/services/authorities/authorities.service";
import {ExpertiseService} from "../../../../../core/services/expertise/expertise.service";
import {ExpertiseClass, ExpertiseSpeciality, ExpertiseType} from "../../../../../core/models/expertise.model";
import {RequestsService} from "../../../../../core/services/requests/requests.service";
import {catchError, filter, map, switchMap, tap} from "rxjs/operators";
import {Router} from "@angular/router";
import {CommissionsService} from "../../../../../core/services/commissions/commissions.service";
import {CommissionDecisionType} from "../../../../../core/models/commission.model";
import {ExpertsService} from "../../../../../core/services/experts/experts.service";
import {RegionsService} from "../../../../../core/services/regions/regions.service";
import {Region} from "../../../../../core/models/region.model";
import {Certificate} from "../../../../../core/models/certificate.model";
import {Expert} from "../../../../../core/models/expert.model";

@Component({
  selector: 'app-request-form-for-heads',
  templateUrl: './request-form-for-heads.component.html',
  styleUrls: ['./request-form-for-heads.component.scss']
})
export class RequestFormForHeadsComponent implements OnInit {
  private subscriptions = new Subscription();
  private isRequestForModificationSubject = new BehaviorSubject<boolean>(false);
  private isNewExpertSubject = new BehaviorSubject<boolean>(false);
  private currentExpertIdSubject = new BehaviorSubject<number | undefined>(undefined);

  @Input() readonly = true;
  @Input() request: Request | undefined = undefined;

  public requestFormGroup!: FormGroup;
  public dateMask = dateMask;
  public possiblyFutureDateMask = possiblyFutureDateMask;
  public phoneMask = phoneMask;

  private expertiseTypesSubject$ = new BehaviorSubject<ExpertiseType[]>([]);
  public expertiseTypes$ = this.expertiseTypesSubject$.asObservable();
  private expertiseSpecialitiesSubject$ = new BehaviorSubject<ExpertiseSpeciality[]>([]);
  public expertiseSpecialities$ = this.expertiseSpecialitiesSubject$.asObservable();

  public isRequestForModification$ = this.isRequestForModificationSubject.asObservable();
  public isNewExpert$ = this.isNewExpertSubject.asObservable();

  public authorities$!: Observable<Authority[]>;
  public expertiseClasses$!: Observable<ExpertiseClass[]>;
  public requestOperationTypes$!: Observable<RequestOperationType[]>;
  public regions$!: Observable<Region[]>;
  public expertCertificates$!: Observable<Certificate[]>;
  public experts$!: Observable<Expert[]>;
  public commissionDecisionTypes$!: Observable<CommissionDecisionType[]>;

  constructor(
    private fb: FormBuilder,
    private alertService: AlertService,
    private usersService: UsersService,
    private expertiseService: ExpertiseService,
    private authoritiesService: AuthoritiesService,
    private requestsService: RequestsService,
    private router: Router,
    private commissionsService: CommissionsService,
    private expertsService: ExpertsService,
    private regionsService: RegionsService
  ) {}

  public ngOnInit(): void {
    this.expertiseTypes$ = this.expertiseService.getAllExpertiseTypes();
    this.expertiseSpecialities$ = this.expertiseService.getAllExpertiseSpecialities();

    this.authorities$ = this.authoritiesService.getAllAuthorities();
    this.expertiseClasses$ = this.expertiseService.getAllExpertiseClasses();
    this.requestOperationTypes$ = this.requestsService.getAllRequestOperationTypes();
    this.regions$ = this.regionsService.getAllRegions();
    this.expertCertificates$ = this.currentExpertIdSubject.pipe(
      switchMap(id => {
        return id ? this.expertsService.getExpertCertificates(id) : of([]);
      })
    );
    this.experts$ = this.expertsService.getAllExperts();
    this.commissionDecisionTypes$ = this.commissionsService.getAllCommissionDecisionTypes();

    this.setUpForm();
    this.setFormForExistingExpert();
  }

  public onSubmit(): void {
    this.subscriptions.add(
      this.requestsService.addNewRequest(this.requestFormGroup.value)
        .pipe(
          catchError(err => {
            return this.handleError(err);
          })
        ).subscribe(() => {
        this.alertService.alertSuccessfulRequestCreation();
        this.router.navigateByUrl('applicants-requests');
      })
    );
  }

  public setFormForNewExpert(): void {
    this.isNewExpertSubject.next(true);
    // this.requestFormGroup.get('expert.id')?.clearValidators();
    // this.requestFormGroup.get('expert.firstName')?.setValidators([Validators.required, ...stringValidators]);
    // this.requestFormGroup.get('expert.lastName')?.setValidators([Validators.required, ...stringValidators]);
  }

  public setFormForExistingExpert(): void {
    this.isNewExpertSubject.next(false);
    // this.requestFormGroup.get('expert.id')?.setValidators([Validators.required]);
    // this.requestFormGroup.get('expert.firstName')?.clearValidators();
    // this.requestFormGroup.get('expert.lastName')?.clearValidators();
  }

  private setUpForm(): void {
    this.setUpFormGroup();

    if (this.readonly) {
      this.makeReadonly();
    } else {
      this.subscribeToChanges();
    }
  }

  private setUpFormGroup(): void {
    this.requestFormGroup = this.fb.group({
      operationTypeId: [this.request?.operationType?.id, Validators.required],
      expert: this.fb.group({
        ...expertFormGroup(
          this.request?.expert.id,
          this.request?.expert.firstName,
          this.request?.expert.lastName,
          this.request?.expert.patronymic
        ),
        contacts: this.fb.group(contactsFormGroup(
          this.request?.expert.contacts.email,
          this.request?.expert.contacts.phone
        )),
        organisation: this.fb.group(organisationFormGroup())
      }),
      commission: this.fb.group(commissionFormGroup(
        this.request?.commission.name,
        this.request?.commission.authority.id
      )),
      commissionDecision: this.fb.group(commissionDecisionFormGroup(
        this.request?.commissionDecision.number,
        this.request?.commissionDecision.date,
        this.request?.commissionDecision.type.id
      )),
      certificate: this.fb.group(certificateFormGroup(
        this.request?.certificate.id,
        this.request?.certificate.documentNumber,
        this.request?.certificate.validityEndDate,
        undefined,
        this.request?.certificate.expertiseType.id,
        this.request?.certificate.expertiseSpecialities.map(speciality => speciality.id)
      )),
      dismissalOrder: this.fb.group(dismissalOrderFormGroup(
        this.request?.dismissalOrderNumber, this.request?.dismissalOrderDate
      ))
    });
  }

  private makeReadonly(): void {
    this.requestFormGroup.disable();
  }

  private subscribeToChanges(): void {
    this.subscriptions.add(
      this.requestFormGroup.get('certificate.expertiseClassId')?.valueChanges.subscribe(classId => {
        this.setExpertiseTypes(classId);
      })
    );

    this.subscriptions.add(
      this.requestFormGroup.get('certificate.expertiseTypeId')?.valueChanges.subscribe(typeId => {
        this.setExpertiseSpecialities(typeId);
      })
    );

    this.subscriptions.add(
      this.requestFormGroup.get('operationTypeId')?.valueChanges.subscribe(id => {
        const isModification = this.requestsService.getOperationTypeName(id) === RequestOperationTypes.MODIFICATION;
        this.setModificationState(isModification);
      })
    );

    this.subscriptions.add(
      this.requestFormGroup.get('expert.id')?.valueChanges.subscribe(id => {
        this.currentExpertIdSubject.next(id);
      })
    );

    this.requestFormGroup.valueChanges.subscribe(() => console.log(this.requestFormGroup.value,
      this.requestFormGroup.errors));
  }

  private setModificationState(isModification: boolean) {
    // if (isModification) {
    //   this.requestFormGroup.get('certificate.id')?.setValidators([Validators.required]);
    //   this.requestFormGroup.get('certificate.documentNumber')?.clearValidators();
    //   this.requestFormGroup.get('certificate.validityEndDate')?.clearValidators();
    //   this.requestFormGroup.get('certificate.expertiseClassId')?.clearValidators();
    //   this.requestFormGroup.get('certificate.expertiseTypeId')?.clearValidators();
    //   this.requestFormGroup.get('certificate.expertiseSpecialitiesId')?.clearValidators();
    //   this.setFormForExistingExpert();
    // } else {
    //   this.requestFormGroup.get('certificate.id')?.clearValidators();
    //   this.requestFormGroup.get('certificate.documentNumber')?.setValidators([
    //     Validators.required, Validators.min(1), Validators.max(1000000)
    //   ]);
    //   this.requestFormGroup.get('certificate.validityEndDate')?.setValidators([Validators.required]);
    //   this.requestFormGroup.get('certificate.expertiseClassId')?.setValidators([Validators.required]);
    //   this.requestFormGroup.get('certificate.expertiseTypeId')?.setValidators([Validators.required]);
    //   this.requestFormGroup.get('certificate.expertiseSpecialitiesId')?.setValidators([Validators.required]);
    //   this.setFormForNewExpert();
    // }
    this.isRequestForModificationSubject.next(
      isModification
    );
  }

  private setExpertiseTypes(classId: number): void {
    this.expertiseTypes$ = this.expertiseService.getAllExpertiseTypes(classId);
  }

  private setExpertiseSpecialities(typeId: number): void {
    this.expertiseSpecialities$ = this.expertiseService.getAllExpertiseSpecialities(typeId);
  }

  private handleError(err: Error): Observable<void> {
    this.alertService.alertError(err);
    return EMPTY;
  }
}

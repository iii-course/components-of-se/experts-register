import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {RequestsService} from "../../../../../core/services/requests/requests.service";
import {Observable} from "rxjs";
import {RequestsSearchTerm, RequestStatus} from "../../../../../core/models/request.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-requests-search-form',
  templateUrl: './requests-search-form.component.html',
  styleUrls: ['./requests-search-form.component.scss']
})
export class RequestsSearchFormComponent implements OnInit {
  @Output() searchTerm = new EventEmitter<RequestsSearchTerm>();

  public formGroup!: FormGroup;

  constructor(private fb: FormBuilder, private requestsService: RequestsService) { }

  public ngOnInit(): void {
    this.formGroup = this.fb.group({
      statusId: [undefined, Validators.required]
    });
  }

  public get requestStatuses$(): Observable<RequestStatus[]> {
    return this.requestsService.getAllRequestsStatuses();
  }

  public onSearch(): void {
    this.searchTerm.emit(this.formGroup.value);
  }
}

import { RequestsSearchFormComponent } from './requests-search-form.component';
import {MockService} from "ng-mocks";
import {RequestsService} from "../../../../../core/services/requests/requests.service";
import {FormBuilder} from "@angular/forms";

describe('RequestsSearchFormComponent', () => {
  let component: RequestsSearchFormComponent;

  beforeEach(() => {
    component = new RequestsSearchFormComponent(new FormBuilder(), MockService(RequestsService));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

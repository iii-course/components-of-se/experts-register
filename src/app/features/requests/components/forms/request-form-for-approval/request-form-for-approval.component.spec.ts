import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestFormForApprovalComponent } from './request-form-for-approval.component';
import {RequestsService} from "../../../../../core/services/requests/requests.service";
import {AlertService} from "../../../../../core/services/alert/alert.service";
import {Router} from "@angular/router";
import {TranslocoService} from "@ngneat/transloco";
import {Mock, MockService} from "ng-mocks";

describe('RequestFormForApprovalComponent', () => {
  let component: RequestFormForApprovalComponent;

  let requestsService: jasmine.SpyObj<RequestsService>;
  let alertService: jasmine.SpyObj<AlertService>;
  let router: jasmine.SpyObj<Router>;

  beforeEach(() => {
    requestsService = MockService(RequestsService) as jasmine.SpyObj<RequestsService>;
    alertService = MockService(AlertService) as jasmine.SpyObj<AlertService>;
    router = MockService(Router) as jasmine.SpyObj<Router>;

    component = new RequestFormForApprovalComponent(
      requestsService,
      alertService,
      router,
      MockService(TranslocoService)
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Request} from "../../../../../core/models/request.model";
import {UserRolesNames} from "../../../../../core/models/user-role.model";
import {RequestsStatuses} from "../../../../../../mock/requests.mock";
import {RequestsService} from "../../../../../core/services/requests/requests.service";
import {AlertService} from "../../../../../core/services/alert/alert.service";
import {Router} from "@angular/router";
import {EMPTY, Observable, Subscription} from "rxjs";
import {catchError, concatMap, take} from "rxjs/operators";
import {TranslocoService} from "@ngneat/transloco";

@Component({
  selector: 'app-request-form-for-approval',
  templateUrl: './request-form-for-approval.component.html',
  styleUrls: ['./request-form-for-approval.component.scss']
})
export class RequestFormForApprovalComponent implements OnInit, OnDestroy {
  @Input() request: Request | undefined = undefined;

  public UserRolesNames = UserRolesNames;
  public RequestsStatuses = RequestsStatuses;

  private approvedStatusId$!: Observable<number | undefined>;
  private rejectedStatusId$!: Observable<number | undefined>;
  private subscription = new Subscription();

  constructor(private requestsService: RequestsService,
              private alertService: AlertService,
              private router: Router,
              private translocoService: TranslocoService
  ) {}

  public ngOnInit(): void {
    this.approvedStatusId$ = this.requestsService.getStatusId(RequestsStatuses.APPROVED);
    this.rejectedStatusId$ = this.requestsService.getStatusId(RequestsStatuses.REJECTED);
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public approve(): void {
    if (this.hasConfirmedApproval()) {
      this.patchApproval();
    }
  }

  public reject(): void {
    if (this.hasConfirmedRejection()) {
      const comment = this.getComment();
      if (this.validateComment(comment)) {
        this.patchRejection(comment as string);
      }
    }
  }

  private patchApproval(): void {
    this.subscription.add(this.approvedStatusId$.pipe(
      take(1),
      concatMap(statusId => this.requestsService.updateRequestStatusById(
        this.request?.id as number, {statusId}
      )),
      catchError(e => this.handleError(e))
      ).subscribe(() => {
        this.alertService.alertSuccessfulRequestApproval();
        this.router.navigateByUrl('requests-queue');
      })
    );
  }

  private patchRejection(comment: string): void {
    this.subscription.add(this.rejectedStatusId$.pipe(
      take(1),
      concatMap(statusId => this.requestsService.updateRequestStatusById(
        this.request?.id as number, {statusId, comment}
      )),
      catchError(e => this.handleError(e))
      ).subscribe(() => {
        this.alertService.alertSuccessfulRequestRejection();
        this.router.navigateByUrl('requests-queue');
      })
    );
  }

  private getComment(): string | null {
    return window.prompt(this.translocoService.translate('provide_comment_for_request_rejection'));
  }

  private validateComment(comment: string | null): boolean {
    if (!comment && (comment as string).length === 0) {
      this.alertService.alertIncorrectComment();
      return false;
    }
    return true;
  }

  private hasConfirmedApproval(): boolean {
    return !!this.request?.id &&
      !!this.approvedStatusId$ &&
      window.confirm(this.translocoService.translate('request_approval_confirmation'));
  }

  private hasConfirmedRejection(): boolean {
    return !!this.request?.id &&
      !!this.approvedStatusId$ &&
      window.confirm(this.translocoService.translate('request_rejection_confirmation'));
  }

  private handleError(err: Error): Observable<void> {
    this.alertService.alertError(err);
    return EMPTY;
  }
}



import {Component, Input, OnInit} from '@angular/core';
import {Request, RequestOperationType, RequestOperationTypes} from "../../../../../core/models/request.model";
import {User} from "../../../../../core/models/user.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BehaviorSubject, EMPTY, Observable, of, Subscription} from "rxjs";
import {UsersService} from "../../../../../core/services/users/users.service";
import {AlertService} from "../../../../../core/services/alert/alert.service";
import {dateMask, phoneMask, possiblyFutureDateMask} from "../../../../../helpers/masks";
import {
  certificateFormGroup,
  commissionDecisionFormGroup,
  commissionFormGroup,
  contactsFormGroup,
  expertFormGroup, organisationFormGroup
} from "../../../helpers/form-groups";
import {Authority} from "../../../../../core/models/authority.model";
import {AuthoritiesService} from "../../../../../core/services/authorities/authorities.service";
import {ExpertiseService} from "../../../../../core/services/expertise/expertise.service";
import {ExpertiseClass, ExpertiseSpeciality, ExpertiseType} from "../../../../../core/models/expertise.model";
import {RequestsService} from "../../../../../core/services/requests/requests.service";
import {catchError, concatMap, map, switchMap, take, tap} from "rxjs/operators";
import {Router} from "@angular/router";
import {ExpertsService} from "../../../../../core/services/experts/experts.service";
import {Expert} from "../../../../../core/models/expert.model";
import {Certificate} from "../../../../../core/models/certificate.model";
import {RegionsService} from "../../../../../core/services/regions/regions.service";
import {Region} from "../../../../../core/models/region.model";


@Component({
  selector: 'app-request-form-for-experts',
  templateUrl: './request-form-for-experts.component.html',
  styleUrls: ['./request-form-for-experts.component.scss']
})
export class RequestFormForExpertsComponent implements OnInit {
  private currentUser!: User;
  private currentUserAsExpert!: Expert | undefined;
  private currentUserAsExpert$!: Observable<Expert | undefined>;
  private subscriptions = new Subscription();
  private isRequestForModificationSubject = new BehaviorSubject<boolean>(false);

  @Input() readonly = true;
  @Input() request: Request | undefined = undefined;

  public requestFormGroup!: FormGroup;
  public dateMask = dateMask;
  public possiblyFutureDateMask = possiblyFutureDateMask;
  public phoneMask = phoneMask;

  public expertiseTypes$!: Observable<ExpertiseType[]>;
  public expertiseSpecialities$!: Observable<ExpertiseSpeciality[]>;

  public isRequestForModification$ = this.isRequestForModificationSubject.asObservable();

  public authorities$!: Observable<Authority[]>;
  public expertiseClasses$!: Observable<ExpertiseClass[]>;
  public requestOperationTypes$!: Observable<RequestOperationType[]>;
  public regions$!: Observable<Region[]>;
  public expertCertificates$!: Observable<Certificate[]>;

  constructor(
    private fb: FormBuilder,
    private alertService: AlertService,
    private usersService: UsersService,
    private expertiseService: ExpertiseService,
    private authoritiesService: AuthoritiesService,
    private requestsService: RequestsService,
    private router: Router,
    private expertsService: ExpertsService,
    private regionsService: RegionsService
  ) {}

  public ngOnInit(): void {
    this.currentUserAsExpert$ = this.usersService.getCurrentUser().pipe(
      tap(user => this.currentUser = user),
      take(1),
      concatMap(user => this.expertsService.getAllExperts({
        firstName: user?.firstName,
        lastName: user?.lastName,
        patronymic: user?.patronymic,
      })),
      map(experts => experts[0])
    );

    this.expertiseTypes$ = this.expertiseService.getAllExpertiseTypes();
    this.expertiseSpecialities$ = this.expertiseService.getAllExpertiseSpecialities();

    this.authorities$ = this.authoritiesService.getAllAuthorities();
    this.expertiseClasses$ = this.expertiseService.getAllExpertiseClasses();
    this.requestOperationTypes$ = this.requestsService.getAllRequestOperationTypes();
    this.regions$ = this.regionsService.getAllRegions();
    this.expertCertificates$ = of(this.currentUserAsExpert?.id).pipe(
      switchMap(id => {
        return id ? this.expertsService.getExpertCertificates(id) : of([]);
      })
    );

    this.setUp();
  }

  public onSubmit(): void {
    this.subscriptions.add(
      this.requestsService.addNewRequest(this.requestFormGroup.value)
        .pipe(
          catchError(err => {
            return this.handleError(err);
          })
        ).subscribe(() => {
          this.alertService.alertSuccessfulRequestCreation();
          this.router.navigateByUrl('applicants-requests');
        })
    );
  }

  private setUp(): void {
    this.subscriptions.add(this.currentUserAsExpert$.subscribe((expert: Expert | undefined) => {
      this.currentUserAsExpert = expert;
      this.setUpForm();
      if (expert) {
        this.requestFormGroup.get('expert.id')?.setValidators([Validators.required]);
        this.requestFormGroup.get('expert.firstName')?.clearValidators();
        this.requestFormGroup.get('expert.lastName')?.clearValidators();
      } else {
        this.requestFormGroup?.get('expert.id')?.clearValidators();
      }
      this.setModificationState(false);
    }));
  }

  private setUpForm(): void {
    this.setUpFormGroup();

    if (this.readonly) {
      this.makeReadonly();
    } else {
      this.subscribeToChanges();
    }

    this.requestFormGroup?.valueChanges.subscribe(() =>console.log(this.requestFormGroup?.value));
  }

  private setUpFormGroup(): void {
    const {currentUser} = this;

    this.requestFormGroup = this.fb.group({
      operationTypeId: [this.request?.operationType?.id, Validators.required],
      expert: this.fb.group({...expertFormGroup(
          this.request?.expert?.id,
          this.request?.expert?.firstName || currentUser.firstName,
          this.request?.expert?.lastName || currentUser.lastName,
          this.request?.expert?.patronymic || currentUser.patronymic,
          true
        ),
        contacts: this.fb.group(contactsFormGroup(
          this.request?.expert.contacts.email || currentUser.email,
          this.request?.expert.contacts.phone
        )),
        organisation: this.fb.group(organisationFormGroup())
      }),
      commission: this.fb.group(commissionFormGroup(
        this.request?.commission.name || 'Експертно-кваліфікаційна комісія',
        this.request?.commission.authority.id
      )),
      commissionDecision: this.fb.group(commissionDecisionFormGroup(
        this.request?.commissionDecision.number,
        this.request?.commissionDecision.date,
        this.request?.commissionDecision.type.id
      )),
      certificate: this.fb.group(certificateFormGroup(
        this.request?.certificate.id,
        this.request?.certificate.documentNumber,
        this.request?.certificate.validityEndDate,
        undefined,
        this.request?.certificate.expertiseType.id,
        this.request?.certificate.expertiseSpecialities.map(speciality => speciality.id)
      ))
    });
  }

  private makeReadonly(): void {
    this.requestFormGroup.disable();
  }

  private subscribeToChanges(): void {
    this.subscriptions.add(
      this.requestFormGroup.get('certificate.expertiseClassId')?.valueChanges.subscribe(classId => {
        this.setExpertiseTypes(classId);
      })
    );

    this.subscriptions.add(
      this.requestFormGroup.get('certificate.expertiseTypeId')?.valueChanges.subscribe(typeId => {
        this.setExpertiseSpecialities(typeId);
      })
    );

    this.subscriptions.add(
      this.requestFormGroup.get('operationTypeId')?.valueChanges.subscribe(id => {
        const isModification = this.requestsService.getOperationTypeName(id) === RequestOperationTypes.MODIFICATION;
        this.setModificationState(isModification);
      })
    );
  }

  private setModificationState(isModification: boolean) {
    if (isModification) {
      this.requestFormGroup.get('certificate.id')?.setValidators([Validators.required]);
      this.requestFormGroup.get('certificate.documentNumber')?.clearValidators();
      this.requestFormGroup.get('certificate.validityEndDate')?.clearValidators();
      this.requestFormGroup.get('certificate.expertiseClassId')?.clearValidators();
      this.requestFormGroup.get('certificate.expertiseTypeId')?.clearValidators();
      this.requestFormGroup.get('certificate.expertiseSpecialitiesId')?.clearValidators();
    } else {
      this.requestFormGroup.get('certificate.id')?.clearValidators();
      this.requestFormGroup.get('certificate.documentNumber')?.setValidators([
        Validators.required, Validators.min(1), Validators.max(1000000)
      ]);
      this.requestFormGroup.get('certificate.validityEndDate')?.setValidators([Validators.required]);
      this.requestFormGroup.get('certificate.expertiseClassId')?.setValidators([Validators.required]);
      this.requestFormGroup.get('certificate.expertiseTypeId')?.setValidators([Validators.required]);
      this.requestFormGroup.get('certificate.expertiseSpecialitiesId')?.setValidators([Validators.required]);
    }
    this.isRequestForModificationSubject.next(
      isModification
    );
  }

  private setExpertiseTypes(classId: number): void {
    this.expertiseTypes$ = this.expertiseService.getAllExpertiseTypes(classId);
  }

  private setExpertiseSpecialities(typeId: number): void {
    this.expertiseSpecialities$ = this.expertiseService.getAllExpertiseSpecialities(typeId);
  }

  private handleError(err: Error): Observable<void> {
    this.alertService.alertError(err);
    return EMPTY;
  }
}

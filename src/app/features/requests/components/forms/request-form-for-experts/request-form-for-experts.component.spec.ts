import { RequestFormForExpertsComponent } from './request-form-for-experts.component';
import {Mock, MockService} from "ng-mocks";
import {FormBuilder, FormGroup} from "@angular/forms";
import {AlertService} from "../../../../../core/services/alert/alert.service";
import {UsersService} from "../../../../../core/services/users/users.service";
import {ExpertiseService} from "../../../../../core/services/expertise/expertise.service";
import {AuthoritiesService} from "../../../../../core/services/authorities/authorities.service";
import {RequestsService} from "../../../../../core/services/requests/requests.service";
import {Router} from "@angular/router";
import {of, throwError} from "rxjs";
import {ExpertsService} from "../../../../../core/services/experts/experts.service";
import {RegionsService} from "../../../../../core/services/regions/regions.service";

describe('RequestFormForExpertsComponent', () => {
  let component: RequestFormForExpertsComponent;

  let alertService: jasmine.SpyObj<AlertService>;
  let usersService: jasmine.SpyObj<UsersService>;
  let expertiseService: jasmine.SpyObj<ExpertiseService>;
  let authoritiesService: jasmine.SpyObj<AuthoritiesService>;
  let requestsService: jasmine.SpyObj<RequestsService>;
  let router: jasmine.SpyObj<Router>;

  beforeEach(() => {
    alertService = MockService(AlertService, {
      alertError: jasmine.createSpy(),
      alertSuccessfulRequestCreation: jasmine.createSpy()
    }) as jasmine.SpyObj<AlertService>;

    usersService = MockService(UsersService, {
      getCurrentUser: jasmine.createSpy().and.returnValue(of({}))
    }) as jasmine.SpyObj<UsersService>;

    expertiseService = MockService(ExpertiseService, {
      getAllExpertiseTypes: jasmine.createSpy().and.returnValue(of([])),
      getAllExpertiseClasses: jasmine.createSpy().and.returnValue(of([])),
      getAllExpertiseSpecialities: jasmine.createSpy().and.returnValue(of([])),
    }) as jasmine.SpyObj<ExpertiseService>;

    authoritiesService = MockService(AuthoritiesService) as jasmine.SpyObj<AuthoritiesService>;
    requestsService = MockService(RequestsService, {
      addNewRequest: jasmine.createSpy().and.returnValue(of(undefined))
    }) as jasmine.SpyObj<RequestsService>;
    router = MockService(Router, {
      navigateByUrl: jasmine.createSpy()
    }) as jasmine.SpyObj<Router>;

    component = new RequestFormForExpertsComponent(
      new FormBuilder(),
      alertService,
      usersService,
      expertiseService,
      authoritiesService,
      requestsService,
      router,
      MockService(ExpertsService),
      MockService(RegionsService)
    );
    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get current user', () => {
    expect(usersService.getCurrentUser).toHaveBeenCalled();
  });

  describe('requestFormGroup', () => {
    let requestFormGroup: FormGroup;

    beforeEach(() => {
      requestFormGroup = component.requestFormGroup;
    });

    it('should be invalid initially', () => {
      expect(requestFormGroup.valid).toBeFalse();
    });

    describe('expert', () => {
      describe('organisation', () => {
        it('should not accept empty organisation name', () => {
          const empty = '';

          const control = requestFormGroup.get('expert.organisation.name');
          control?.setValue(empty);

          expect(control?.valid).toBeFalse();
        });

        it('should not accept numeric organisation name', () => {
          const numeric = '123';

          const control = requestFormGroup.get('expert.organisation.name');
          control?.setValue(numeric);

          expect(control?.valid).toBeFalse();
        });
      });
    });
  });

  describe('onSubmit', () => {
    beforeEach(() => {
      component.onSubmit();
    });

    it('should add new request', () => {
      expect(requestsService.addNewRequest).toHaveBeenCalled();
    });

    it('should alert successful request creation', () => {
      expect(alertService.alertSuccessfulRequestCreation).toHaveBeenCalled();
    });

    it('should navigate to /applicants-requests', () => {
      expect(router.navigateByUrl).toHaveBeenCalledOnceWith('applicants-requests');
    });

    describe('when error has happened', () => {
      beforeEach(() => {
        requestsService.addNewRequest = jasmine.createSpy().and.returnValue(throwError(new Error()));
        component.onSubmit();
      });

      it('should alert error', () => {
        expect(alertService.alertError).toHaveBeenCalled();
      });
    });
  });
});

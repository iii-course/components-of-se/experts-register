import {Component, Input} from '@angular/core';
import {AuthService} from "../../../../../core/services/auth/auth.service";
import {UserRole, UserRolesNames} from "../../../../../core/models/user-role.model";
import {Observable} from "rxjs";
import {Request} from "../../../../../core/models/request.model";

@Component({
  selector: 'app-request-form',
  templateUrl: './request-form.component.html',
  styleUrls: ['./request-form.component.scss']
})
export class RequestFormComponent {
  @Input() request: Request | undefined = undefined;
  @Input() readonly = true;

  public UserRolesNames = UserRolesNames;

  constructor(private authService: AuthService) { }

  public get userRole$(): Observable<UserRole> {
    return this.authService.userRole$;
  }
}

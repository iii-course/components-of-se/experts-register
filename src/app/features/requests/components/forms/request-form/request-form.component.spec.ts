import { RequestFormComponent } from './request-form.component';
import {MockService} from "ng-mocks";
import {AuthService} from "../../../../../core/services/auth/auth.service";

describe('RequestFormComponent', () => {
  let component: RequestFormComponent;

  beforeEach(() => {
    component = new RequestFormComponent(MockService(AuthService));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

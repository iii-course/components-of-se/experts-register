import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicantRequestsTableComponent } from './components/tables/applicant-requests-table/applicant-requests-table.component';
import {SharedModule} from "../../shared/shared.module";
import { RequestFormComponent } from './components/forms/request-form/request-form.component';
import { RequestFormForExpertsComponent } from './components/forms/request-form-for-experts/request-form-for-experts.component';
import { RequestFormForHeadsComponent } from './components/forms/request-form-for-heads/request-form-for-heads.component';
import {IMaskModule} from "angular-imask";
import { RequestsTableComponent } from './components/tables/requests-table/requests-table.component';
import { RequestFormForApprovalComponent } from './components/forms/request-form-for-approval/request-form-for-approval.component';
import { RequestsSearchFormComponent } from './components/forms/requests-search-form/requests-search-form.component';


@NgModule({
  declarations: [
    ApplicantRequestsTableComponent,
    RequestFormComponent,
    RequestFormForExpertsComponent,
    RequestFormForHeadsComponent,
    RequestsTableComponent,
    RequestFormForApprovalComponent,
    RequestsSearchFormComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    IMaskModule
  ],
  exports: [
    ApplicantRequestsTableComponent,
    RequestFormComponent,
    RequestsTableComponent,
    RequestsSearchFormComponent,
  ]
})
export class RequestsModule { }

import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Observable} from "rxjs";
import {LogsSearchTerm, TypeOfChange} from "../../../../core/models/log.model";
import {UsersService} from "../../../../core/services/users/users.service";
import {LogsService} from "../../../../core/services/logs/logs.service";
import {UserRole} from "../../../../core/models/user-role.model";
import {dateMask} from "../../../../helpers/masks";
import {stringValidators} from "../../../../helpers/validators";
import {UsersSearchTerm} from "../../../../core/models/user.model";

@Component({
  selector: 'app-logs-search-form',
  templateUrl: './logs-search-form.component.html',
  styleUrls: ['./logs-search-form.component.scss']
})
export class LogsSearchFormComponent implements OnInit {
  @Output() searchTerm = new EventEmitter<LogsSearchTerm>();
  public searchFormGroup!: FormGroup;
  public dateMask = dateMask;

  public roles$!: Observable<UserRole[]>;
  public typesOfChanges$!: Observable<TypeOfChange[]>;

  constructor(
    private fb: FormBuilder,
    private usersService: UsersService,
    private logsService: LogsService
  ) {}

  public ngOnInit(): void {
    this.roles$ = this.usersService.getUsersRoles();
    this.typesOfChanges$ = this.logsService.getTypesOfChanges();
    this.setUpForm();
  }

  public onSubmit(): void {
    this.searchTerm.emit(this.getSearchTerm());
  }

  private getSearchTerm(): LogsSearchTerm {
    const searchTerm = new Map();
    Object.entries(this.searchFormGroup.value)
      .filter(([_, value]) => !!value)
      .forEach(([key, value]) => searchTerm.set(key, value));
    return Object.fromEntries(searchTerm) as LogsSearchTerm;
  }

  private setUpForm() {
    this.searchFormGroup = this.fb.group({
      authorFirstName: ['', stringValidators],
      authorLastName: ['', stringValidators],
      authorPatronymic: ['', stringValidators],
      authorRoleId: null,
      affectedUserFirstName: ['', stringValidators],
      affectedUserLastName: ['', stringValidators],
      affectedUserPatronymic: ['', stringValidators],
      affectedUserRoleId: null,
      date: '',
      typeOfChangeId: null,
      // requestId: null,
      // requestTypeId: null,
      // requestDate: '',
    });
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogsSearchFormComponent } from './logs-search-form.component';
import {FormBuilder} from "@angular/forms";
import {MockService} from "ng-mocks";
import {UsersService} from "../../../../core/services/users/users.service";
import {LogsService} from "../../../../core/services/logs/logs.service";

describe('LogsSearchFormComponent', () => {
  let component: LogsSearchFormComponent;

  beforeEach(() => {
    component = new LogsSearchFormComponent(
      new FormBuilder(),
      MockService(UsersService),
      MockService(LogsService),
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Observable, of} from "rxjs";
import {calculatePaginationParameters, PaginationParameters} from "../../../../helpers/pagination";
import {PageEvent} from "@angular/material/paginator";
import {LogsService} from "../../../../core/services/logs/logs.service";
import {Log, LogsSearchTerm} from "../../../../core/models/log.model";
import {distinctUntilChanged} from "rxjs/operators";

@Component({
  selector: 'app-logs-table',
  templateUrl: './logs-table.component.html',
  styleUrls: ['./logs-table.component.scss']
})
export class LogsTableComponent implements OnInit, OnChanges {
  @Input() parameters!: LogsSearchTerm | null;
  public logs$: Observable<Log[] | undefined> = of(undefined);
  public logsTotalCount$: Observable<number | undefined> = of(undefined);
  public columnsToDisplay = ['dateOfChange', 'typeOfChange', 'author', 'affectedUser', 'request'];

  private paginationParams: PaginationParameters = {
    limit: 10,
    offset: 0
  };

  constructor(private logsService: LogsService) { }

  public ngOnInit(): void {
    this.updateLogs();
  }

  public ngOnChanges(): void {
    this.updateLogs();
  }

  public onPageSelected(event: PageEvent): void {
    this.paginationParams = calculatePaginationParameters(event);
    this.logs$ = this.logsService.getAllLogs({...this.parameters, ...this.paginationParams});
  }

  private updateLogs(): void {
    const params = {...this.parameters, ...this.paginationParams};
    this.logs$ = this.logsService.getAllLogs(params);
    this.logsTotalCount$ = this.logsService.getLogsTotalCount(params).pipe(distinctUntilChanged());
  }
}

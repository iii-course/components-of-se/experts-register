import { LogsTableComponent } from './logs-table.component';
import {MockService} from "ng-mocks";
import {LogsService} from "../../../../core/services/logs/logs.service";

describe('LogsTableComponent', () => {
  let component: LogsTableComponent;

  beforeEach(() => {
    component = new LogsTableComponent(MockService(LogsService));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

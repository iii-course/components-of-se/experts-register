import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogsSearchFormComponent } from './components/logs-search-form/logs-search-form.component';
import {SharedModule} from "../../shared/shared.module";
import {IMaskModule} from "angular-imask";
import { LogsTableComponent } from './components/logs-table/logs-table.component';

@NgModule({
    declarations: [
      LogsSearchFormComponent,
      LogsTableComponent
    ],
  exports: [
    LogsSearchFormComponent,
    LogsTableComponent
  ],
    imports: [
      IMaskModule,
      CommonModule,
      SharedModule
    ]
})
export class LogsModule { }

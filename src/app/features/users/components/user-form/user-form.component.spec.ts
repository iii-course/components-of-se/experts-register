import { UserFormComponent } from './user-form.component';
import {AbstractControl, FormBuilder} from "@angular/forms";
import {MockService} from "ng-mocks";
import {UsersService} from "../../../../core/services/users/users.service";
import {AlertService} from "../../../../core/services/alert/alert.service";

describe('UserFormComponent', () => {
  let component: UserFormComponent;

  beforeEach(() => {
    component = new UserFormComponent(
      new FormBuilder(),
      MockService(UsersService),
      MockService(AlertService),
    );
    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('userFormGroup', () => {
    it('should not be valid initially', () => {
      expect(component.userFormGroup?.valid).toBeFalse();
    });

    describe('firstName', () => {
      let firstNameControl: AbstractControl | null;

      beforeEach(() => {
        firstNameControl = component.userFormGroup?.get('firstName');
        firstNameControl?.setValue('Valid Name');
      });

      it('should not accept empty first name', () => {
        const empty = '';
        firstNameControl?.setValue(empty);

        expect(firstNameControl?.valid).toBeFalse();
      });

      it('should not accept digits in first name', () => {
        const digits = '123';
        firstNameControl?.setValue(digits);

        expect(firstNameControl?.valid).toBeFalse();
      });

      it('should not accept special characters in first name', () => {
        const specialCharacters = '!@#$%^&*()/,.<>{}[]';

        for (const char of specialCharacters.split('')) {
          firstNameControl?.setValue(char);

          expect(firstNameControl?.valid).toBeFalse();
        }
      });
    });

    describe('lastName', () => {
      let lastNameControl: AbstractControl | null;

      beforeEach(() => {
        lastNameControl = component.userFormGroup?.get('lastName');
        lastNameControl?.setValue('Valid Name');
      });

      it('should not accept empty first name', () => {
        const empty = '';
        lastNameControl?.setValue(empty);

        expect(lastNameControl?.valid).toBeFalse();
      });

      it('should not accept digits in first name', () => {
        const digits = '123';
        lastNameControl?.setValue(digits);

        expect(lastNameControl?.valid).toBeFalse();
      });

      it('should not accept special characters in first name', () => {
        const specialCharacters = '!@#$%^&*()/,.<>{}[]';

        for (const char of specialCharacters.split('')) {
          lastNameControl?.setValue(char);

          expect(lastNameControl?.valid).toBeFalse();
        }
      });
    });

    describe('email', () => {
      let emailControl: AbstractControl | null;

      beforeEach(() => {
        emailControl = component.userFormGroup?.get('email');
        emailControl?.setValue('valid@email.com');
      });

      it('should not accept invalid email', () => {
        emailControl?.setValue('invalid');

        expect(emailControl?.valid).toBeFalse();
      });
    });
  });
});

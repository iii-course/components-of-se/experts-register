import {Component, OnInit, Input, OnChanges, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UsersService} from "../../../../core/services/users/users.service";
import {EMPTY, Observable, Subscription} from "rxjs";
import {UserRole} from "../../../../core/models/user-role.model";
import {User} from "../../../../core/models/user.model";
import {
  dateMask,
  passportAuthorityCodeMask, passportNumberMask, passportSeriesMask,
  taxpayerNumberMask
} from "../../../../helpers/masks";
import {catchError} from "rxjs/operators";
import {AlertService} from "../../../../core/services/alert/alert.service";
import {passwordValidators, stringValidators} from "../../../../helpers/validators";

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit, OnChanges {
  @Input() user!: User | null;
  public userFormGroup!: FormGroup;
  public passportFormGroup!: FormGroup;

  public roles$!: Observable<UserRole[]>;

  public dateMask = dateMask;
  public passportAuthorityCodeMask = passportAuthorityCodeMask;
  public passportNumberMask = passportNumberMask;
  public passportSeriesMask = passportSeriesMask;
  public taxpayerNumberMask = taxpayerNumberMask;

  private subscriptions = new Subscription();
  private isUserSet = false;

  constructor(
    private fb: FormBuilder,
    private usersService: UsersService,
    private alertService: AlertService
  ) {}

  public ngOnInit(): void {
    this.setUpForm();
    this.roles$ = this.usersService.getUsersRoles();
  }

  public ngOnChanges(): void {
    if (this.user && !this.isUserSet) {
      this.isUserSet = true;
      this.updateFormValues();
    }
  }

  public onSave(): void {
    if (this.user) {
      this.updateUser();
    } else {
      this.addNewUser();
    }
  }

  public onActivationToggle(): void {
    if (this.user) {
      this.subscriptions.add(
        this.usersService.updateUserById(
          this.user.id, {...this.user, roleId: this.user.role.id, isActive: !this.user.isActive}
          )
          .subscribe(updatedUser => {
            this.user = updatedUser;
            this.updateFormValues();
          })
      );
    }
  }

  private updateUser(): void {
    if (this.user) {
      this.subscriptions.add(
        this.usersService.updateUserById(
          this.user.id, {...this.user, roleId: this.user.role.id, ...this.userFormGroup.value}
        ).pipe(
            catchError(err => {
              return this.handleError(err);
            })
          ).subscribe(updatedUser => {
            if (updatedUser) {
              this.alertService.alertSuccessfulAccountUpdate();
              this.user = updatedUser;
              this.updateFormValues();
            }
          })
      );
    }
  }

  private addNewUser(): void {
    this.subscriptions.add(
      this.usersService.addNewUser(this.userFormGroup.value)
        .pipe(
          catchError(err => {
            return this.handleError(err);
          })
        ).subscribe(() => this.alertService.alertSuccessfulRegistration())
    );
  }

  private setUpForm(): void {
    this.passportFormGroup = this.fb.group({
      number: [this.user?.passport.number || '', Validators.required],
      series: this.user?.passport.series || '',
      dateOfIssue: [this.user?.passport.dateOfIssue || '', Validators.required],
      authorityCode: [this.user?.passport.authorityCode || '', Validators.required],
    });

    this.userFormGroup = this.fb.group({
      firstName: [this.user?.firstName || '', [Validators.required, ...stringValidators]],
      lastName: [this.user?.lastName || '', [Validators.required, ...stringValidators]],
      patronymic: [this.user?.patronymic || '', stringValidators],
      roleId: [this.user?.role.id || null, Validators.required],
      birthdate: [this.user?.birthdate || '', Validators.required],
      passport: this.passportFormGroup,
      taxpayerRegistrationNumber: this.user?.taxpayerRegistrationNumber,
      email: [this.user?.email || '', [Validators.required, Validators.email]],
      password: ['', [Validators.required, ...passwordValidators]]
    });
  }

  private updateFormValues() {
    if (this.userFormGroup && this.user) {
      this.userFormGroup.controls['firstName'].setValue(this.user?.firstName);
      this.userFormGroup.controls['lastName'].setValue(this.user?.lastName);
      this.userFormGroup.controls['patronymic'].setValue(this.user?.patronymic);
      this.userFormGroup.controls['roleId'].setValue(this.user?.role.id);

      this.userFormGroup.controls['birthdate'].setValue(this.user?.birthdate);
      this.userFormGroup.controls['taxpayerRegistrationNumber'].setValue(this.user?.taxpayerRegistrationNumber);
      this.userFormGroup.controls['email'].setValue(this.user?.email);

      this.passportFormGroup.controls['number'].setValue(this.user?.passport.number);
      this.passportFormGroup.controls['series'].setValue(this.user?.passport.series);
      this.passportFormGroup.controls['dateOfIssue'].setValue(this.user?.passport.dateOfIssue);
      this.passportFormGroup.controls['authorityCode'].setValue(this.user?.passport.authorityCode);
    }
  }

  private handleError(err: Error): Observable<void> {
    this.alertService.alertError(err);
    return EMPTY;
  }
}

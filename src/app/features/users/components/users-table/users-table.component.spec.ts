import { UsersTableComponent } from './users-table.component';
import {MockService} from "ng-mocks";
import {UsersService} from "../../../../core/services/users/users.service";
import {Router} from "@angular/router";

describe('UsersTableComponent', () => {
  let component: UsersTableComponent;

  beforeEach(() => {
    component = new UsersTableComponent(
      MockService(UsersService),
      MockService(Router),
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

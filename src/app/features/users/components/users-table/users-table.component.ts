import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Observable, of} from "rxjs";
import {calculatePaginationParameters, PaginationParameters} from "../../../../helpers/pagination";
import {PageEvent} from "@angular/material/paginator";
import {UsersService} from "../../../../core/services/users/users.service";
import {User, UsersSearchTerm} from "../../../../core/models/user.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss']
})
export class UsersTableComponent implements OnInit, OnChanges {
  @Input() parameters!: UsersSearchTerm | null;
  public users$: Observable<User[] | undefined> = of(undefined);
  public usersTotalCount$: Observable<number | undefined> = of(undefined);
  public columnsToDisplay = ['lastName', 'firstName', 'patronymic', 'role', 'account'];

  private paginationParams: PaginationParameters = {
    limit: 10,
    offset: 0
  };

  constructor(
    private usersService: UsersService,
    private router: Router
  ) { }

  public ngOnInit(): void {
    this.updateUsers();
  }

  public ngOnChanges(): void {
    this.updateUsers();
  }

  public onPageSelected(event: PageEvent): void {
    this.paginationParams = calculatePaginationParameters(event);
    const params = {...this.parameters, ...this.paginationParams};
    this.users$ = this.usersService.getAllUsers(params);
  }

  public onUserSelected(user: User): void {
    void this.router.navigateByUrl(`users/${user.id}`);
  }

  private updateUsers(): void {
    const params = {...this.parameters, ...this.paginationParams};
    console.log(params);
    this.users$ = this.usersService.getAllUsers(params);
    this.usersTotalCount$ = this.usersService.getUsersTotalCount(params);
  }
}

import { UsersSearchFormComponent } from './users-search-form.component';
import {MockService} from "ng-mocks";
import {UsersService} from "../../../../core/services/users/users.service";
import {FormBuilder} from "@angular/forms";

describe('UsersSearchFormComponent', () => {
  let component: UsersSearchFormComponent;

  beforeEach(() => {
    component = new UsersSearchFormComponent(
      new FormBuilder(),
      MockService(UsersService),
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

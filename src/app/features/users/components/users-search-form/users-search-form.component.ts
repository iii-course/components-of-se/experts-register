import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Observable} from "rxjs";
import {UsersService} from "../../../../core/services/users/users.service";
import {UserRole} from "../../../../core/models/user-role.model";
import {UsersSearchTerm} from "../../../../core/models/user.model";
import {stringValidators} from "../../../../helpers/validators";
import {shareReplay, take} from "rxjs/operators";

@Component({
  selector: 'app-users-search-form',
  templateUrl: './users-search-form.component.html',
  styleUrls: ['./users-search-form.component.scss']
})
export class UsersSearchFormComponent implements OnInit {
  @Output() searchTerm = new EventEmitter<UsersSearchTerm>();
  public searchFormGroup!: FormGroup;
  public roles$!: Observable<UserRole[]>;

  constructor(
    private fb: FormBuilder,
    private usersService: UsersService,
  ) {}

  public ngOnInit(): void {
    this.setUpForm();
    this.roles$ = this.usersService.getUsersRoles();
  }

  public onSubmit(): void {
    this.searchTerm.emit(this.getSearchTerm());
  }

  private getSearchTerm(): UsersSearchTerm {
    const searchTerm = new Map();
    Object.entries(this.searchFormGroup.value)
      .filter(([_, value]) => !!value)
      .forEach(([key, value]) => searchTerm.set(key, value));
    return Object.fromEntries(searchTerm) as UsersSearchTerm;
  }

  private setUpForm(): void {
    this.searchFormGroup = this.fb.group({
      firstName: ['', stringValidators],
      lastName: ['', stringValidators],
      patronymic: ['', stringValidators],
      roleId: null,
    });
  }
}

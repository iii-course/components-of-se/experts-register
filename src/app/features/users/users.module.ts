import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersSearchFormComponent } from './components/users-search-form/users-search-form.component';
import {SharedModule} from "../../shared/shared.module";
import { UsersTableComponent } from './components/users-table/users-table.component';
import { UserFormComponent } from './components/user-form/user-form.component';
import {IMaskModule} from "angular-imask";

@NgModule({
  declarations: [
    UsersSearchFormComponent,
    UsersTableComponent,
    UserFormComponent
  ],
  exports: [
    UsersSearchFormComponent,
    UsersTableComponent,
    UserFormComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    IMaskModule
  ]
})
export class UsersModule { }

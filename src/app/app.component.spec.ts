import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {MockService} from "ng-mocks";
import {AuthService} from "./core/services/auth/auth.service";

describe('AppComponent', () => {
  let component: AppComponent;

  beforeEach(() => {
    component = new AppComponent(MockService(AuthService));
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });
});

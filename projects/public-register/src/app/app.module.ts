import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {SharedModule} from "../../../../src/app/shared/shared.module";
import {ExpertsModule} from "../../../../src/app/features/experts/experts.module";
import {ExpertsPageComponent} from "../../../../src/app/core/pages/experts-page/experts-page.component";
import {HttpClientModule} from "@angular/common/http";
import {CommonModule} from "@angular/common";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TranslocoModule} from "@ngneat/transloco";
import {TranslocoRootModule} from "../../../../src/app/transloco-root.module";

@NgModule({
  declarations: [
    AppComponent,
    ExpertsPageComponent
  ],
    imports: [
      CommonModule,
      BrowserModule,
      AppRoutingModule,
      BrowserAnimationsModule,
      HttpClientModule,
      SharedModule,
      ExpertsModule,
      TranslocoRootModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

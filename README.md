# ExpertsRegister

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.4.

## Usage

### public-register
Provides access to experts data for all users (no authorization needed).

### register

Is designed only for authorized users to gain access to CRUD tools regarding experts data. The following emails are provided _only for demonstration purposes_. At the moment, all data _is mocked_.
* admin@example.com (to log in as an admin)
* expert@example.com (to log in as an expert)
* head@example.com (to log in as a head)
* registrar@example.com (to log in as a registrar)
You may type any password.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
